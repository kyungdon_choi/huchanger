################################
# Main Author : KyungDon Choi
################################

# Python built-in functions
import sys
import os
import platform
import math
import shutil

# PyQt4 import
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import PyQt4.Qt
from PyQt4 import uic
from PyQt4.QtGui import QImage
from PyQt4 import QtGui, QtCore

import matplotlib
from matplotlib import pyplot,cm

class contour_part(object):
    def __init__(self):
        super(contour_part, self)
        self.Contour_Data = []
        self.ROI_Name_List = []
        self.contour_data_points = {}
        self.ROI_EXT_NAME = ''
        self.SKIN_contours = []
    
        self.colors = []
        self.color = QColor(0, 0, 0, 255)

        self.linex = []
        self.liney = []
        self.tot_contour = []
        self.color_matplotlib = (0, 0, 0)

        self.ROI_name = ''
    def contour_list_get(self, ds_struct):
        self.ROI_Name_List = []
        self.Contour_Data = []
        for i in range(0, len(ds_struct.StructureSetROISequence)):
            self.ROI_Name_List.append(ds_struct.StructureSetROISequence[i].ROIName)
            for j in range(0, len(ds_struct.StructureSetROISequence)):
                if ds_struct.StructureSetROISequence[i].ROINumber == ds_struct.ROIContourSequence[j].ReferencedROINumber:
                    self.Contour_Data.append(ds_struct.ROIContourSequence[j])
        
    def Get_contouring_geo(self):
        for i in range(0, len(self.ROI_Name_List)):
            self.tpoints = []
            for j in range(0, len(self.Contour_Data[i].Contours)):
                self.tpoints.append(self.Contour_Data[i].Contours[j].ContourData)
            self.contour_data_points[self.ROI_Name_List[i]] = self.tpoints

    def Get_External_contour(self, ds_struct, p_su, size, IPP):
        for i in range(0,len(ds_struct.RTROIObservationsSequence)):
                if ds_struct.RTROIObservationsSequence[i].RTROIInterpretedType == 'EXTERNAL':
                    for j in range(0, len(ds_struct.StructureSetROISequence)):
                        if ds_struct.RTROIObservationsSequence[i].ReferencedROINumber == ds_struct.StructureSetROISequence[j].ROINumber:
                            self.ROI_EXT_NAME = ds_struct.StructureSetROISequence[j].ROIName
                            self.SKIN_contours = self.contour_data_points[self.ROI_EXT_NAME]
                            '''if p_su == -1:
                                for con_list in range(0, len(self.SKIN_contours)):
                                    for con_check in range(0, len(self.SKIN_contours[con_list])):
                                        if con_check % 3 == 0:
                                            self.SKIN_contours[con_list][con_check] = size[0] - (self.SKIN_contours[con_list][con_check] - IPP[0]) - IPP[0]
                                        elif con_check % 3 == 1:
                                            self.SKIN_contours[con_list][con_check] = size[1] - (self.SKIN_contours[con_list][con_check] - IPP[1]) - IPP[1]'''
                            break

    def Check_ROI_Name(self, ROI_name, ds_struct):
        for i in range(0,len(self.ROI_Name_List)):
            if self.ROI_Name_List[i] == ROI_name:
                self.ROI_name = str(ROI_name)
                self.colors = ds_struct.ROIContourSequence[i].ROIDisplayColor
                self.color = QColor(self.colors[0], self.colors[1], self.colors[2], 255)
                self.color_matplotlib = (float(self.colors[0])/255.0, float(self.colors[1])/255.0, float(self.colors[2])/255.0)
                break

    def Contour_Line_Construction(self, Scroll_value, minloc, z_min_grid, y_min, x_min, position, p_su, size, spacing):
        self.tot_contour = []
        for contour_array in range(0, len(self.contour_data_points[str(self.ROI_name)])):
            if int((self.contour_data_points[self.ROI_name][contour_array][2] - minloc) / spacing[2]) == int(Scroll_value):
                self.linex = []
                self.liney = []
                for pointsnum in range(0, len(self.contour_data_points[self.ROI_name][contour_array])):
                    if p_su == 1:                
                        if pointsnum % 3 == 0:
                            self.linex.append((self.contour_data_points[self.ROI_name][contour_array][pointsnum] - position[0]) * p_su)
                        elif pointsnum % 3 == 1:
                                self.liney.append((self.contour_data_points[self.ROI_name][contour_array][pointsnum] - position[1]) * p_su)
                    elif p_su == -1:
                        if pointsnum % 3 == 0:
                            self.linex.append((self.contour_data_points[self.ROI_name][contour_array][pointsnum] - position[0]) * p_su)
                        elif pointsnum % 3 == 1:
                            self.liney.append((self.contour_data_points[self.ROI_name][contour_array][pointsnum] - position[1]) * p_su)

                self.tot_contour.append([self.linex, self.liney])

    def Get_total_contour(self, ROI_name):
        self.ROI_total_contour = self.contour_data_points[ROI_name]