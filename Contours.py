import cython
cimport cython
import numpy
import shapely
from shapely.geometry import Polygon
import shapely.speedups
from cython.parallel import prange, parallel

def contourchange(Xarray, Yarray, temparray,int zpos, pixd, int vals):
    cdef int size = len(Xarray)
    cdef int k = 0
    cdef int i = 0
    cdef int j = 0
    cdef double x = 0.0
    cdef double y = 0.0
    #cdef double[:,::1] Pointsets = numpy.zeros((size,2), float)
    Pointsets = [] 
    cdef double[4] polydat = numpy.zeros(4, float)
    cdef int[4] rpolydat = numpy.zeros(4, int)

    if size < 3:
        return
    
    else:
        for k in range(0, size):
            Pointsets.append((Xarray[k],Yarray[k]))
        poly = Polygon(Pointsets)
        rpolydat[0] = int(numpy.amin(Xarray)/pixd[0])
        rpolydat[1] = int(numpy.amin(Yarray)/pixd[1])
        rpolydat[2] = int(numpy.amax(Xarray)/pixd[0]) + 1
        rpolydat[3] = int(numpy.amax(Yarray)/pixd[1]) + 1
        
        for j in range(rpolydat[1], rpolydat[3], 1):
            for i in range(rpolydat[0], rpolydat[2], 1):
                x = i*pixd[0]
                y = j*pixd[1]
                z = 0
                testpoint = shapely.geometry.Point(x, y, z)
                if poly.contains(testpoint):
                    temparray[zpos,j,i] = vals


def matchanger(Mat1, Mat2):
    cdef int z = 0
    cdef int y = 0
    cdef int x = 0
    for z in range(0, Mat1.shape[0]):
        for y in range(0, Mat1.shape[1]):
            for x in range(0, Mat1.shape[2]):
                Mat2[z, y, x] = Mat1[z, y, x]
                
@cython.boundscheck(False)
@cython.wraparound(False)                     
def CTChange(short[:,:,::1] OArray, int[:,:,::1] RArray,int RS,int RI):
    cdef int Tempval = 0
    cdef int z = 0
    cdef int y = 0
    cdef int x = 0
    
    for z in range(0, OArray.shape[0]):
        for y in range(0, OArray.shape[1]):
            for x in range(0, OArray.shape[2]):
                Tempval = OArray[z, y, x] * RS + RI
                if Tempval < -1024:
                    RArray[z, y, x] = -1024
                else:
                    RArray[z, y, x] = Tempval

def OuterSkin(CTmap, CheckMat):
    cdef int z = 0
    cdef int y = 0
    cdef int x = 0
    for z in range(0, CheckMat.shape[0]):
        for y in range(0, CheckMat.shape[1]):
            for x in range(0, CheckMat.shape[2]):
                if CheckMat[z, y, x] == 0:
                    CTmap[z, y, x] = -1024
