import os
import dicom
import sys
import platform
import math
import shutil
from os.path import join as pjoin
from decimal import Decimal
import struct

if struct.calcsize("P") == 4:
    import Contours as CT
elif struct.calcsize("P") == 8:
    import Contours_64bit as CT

import numpy

class dicom_load(object):
    def __init__(self):
        self.ct_path = 'None'              # CT path initialize
        self.struct_path = 'None'          # STRUCT path initialize
        self.plan_path = 'None'            # Plan path initialize

        self.pnamect = ''                   # Patient name in CT
        self.pnamest = ''                   # Patient name in STRUCT
        self.pnamepl = ''                   # Patient name in PLAN
        self.openct = ''                    # CT path construction string
        self.path = ''                      # Path to CT

        self.filelist = []                  # All files list in Path
        self.ctlist = []                    # All CT files list in path

        self.ds = ''                        # Temporary variable for CT dicom selection
        self.dsstruct = ''                  # RT_STRUCT
        self.dsplan = ''                    # RT_PLAN

        self.minloc = 999999                # Temp min location for the calculation
        self.tminloc = 0                    # Temporary min location to compare with self.minloc
        self.row = 0                        # Read row size from CT
        self.column = 0                     # Read column size from CT
        self.height = 0                     # Read height size from number of CT slices
        self.thickn = 0                     # Get the CT thickness
        self.position = []                  # Get Image Position Patient for future use
        self.ctmap = numpy.zeros((1, 1, 1), numpy.int16)  # Construct a array for CT map

        self.PixelSpacing = []              # Construct PixelSpacing array
        self.size = []                      # CT size in mm

        self.RI = 0.0                       # Rescale Intercept
        self.RS = 0.0                       # Rescale Slope

        self.proneorsurf = 1                # Prone or Supine tag

    def get_CT_path(self, path):
        self.ct_path = path
    def get_STRUCT_path(self, path):
        self.struct_path = path
    def get_PLAN_path(self, path):
        self.plan_path = path

    def CT_load(self):
        # Check if it is a dicom file or not and generate a flag to run if it is true
        if self.ct_path == 'None':
            return 'No path'
        try:
            self.dsCT = dicom.read_file(self.ct_path)
        except:
            return 'Not dicom'

        # If it is a CT Image
        if self.dsCT.SOPClassUID == '1.2.840.10008.5.1.4.1.1.2':
            # Get patient name inside the CT file
            self.pnamect = self.dsCT.PatientName
            self.RI = self.dsCT.RescaleIntercept
            self.RS = self.dsCT.RescaleSlope
            self.openct = self.ct_path.split('/')  # split the path with / to reconstruct the CT file path
            self.filelist = []
            self.ctlist = []

            ######################## Construct the path depend on the system  ######################
            if platform.system() == 'Windows':
                for i in range(0, len(self.openct) - 1):
                    self.path = self.path + self.openct[i] + '/'
            elif platform.system() == 'Linux':
                self.path = '/'
                for i in range(0, len(self.openct) - 1):
                    self.path = self.path + self.openct[i] + '/'
            ##########################################################################################

            ##################  All file list in path folder ##################
            for dirName, subdirList, fileList in os.walk(self.path):
                for filename in fileList:
                    self.filelist.append(os.path.join(dirName, filename))
                break
            #####################################################################

            ##################  All CT list in path folder ##################
            for lstFiles in self.filelist:
                self.ds = dicom.read_file(lstFiles, force=True)
                if self.ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.2':  # CT Image
                    self.ctlist.append(os.path.join(lstFiles))
            #####################################################################

            self.row = int(self.dsCT.Rows)
            self.column = int(self.dsCT.Columns)
            self.height = len(self.ctlist)
            self.thickn = self.dsCT.SliceThickness
            self.position = self.dsCT.ImagePositionPatient
            self.ctmap = numpy.zeros((self.height, self.column, self.row), numpy.int16)

            self.PixelSpacing = [self.dsCT.PixelSpacing[0], self.dsCT.PixelSpacing[1], self.thickn]
            self.size = [self.ctmap.shape[2] * self.PixelSpacing[0], self.ctmap.shape[1] * self.PixelSpacing[1],
                         self.ctmap.shape[0] * self.PixelSpacing[2]]

            #################### Calculate minimum location to align CT image ##################
            self.minloc = 999999
            for i in self.ctlist:
                self.dstemp = dicom.read_file(i)
                self.tminloc = self.dstemp.SliceLocation
                if self.tminloc < self.minloc:
                    self.minloc = self.tminloc
            #####################################################################################

            ################################ CT matrix stored ###################################
            for i in self.ctlist:
                self.dstemp = dicom.read_file(i)
                self.ctmap[int((self.dstemp.SliceLocation - self.minloc) / self.thickn), :,:] = self.dstemp.pixel_array
            #####################################################################################

            ############################# CT renorm for WEPL calculation ########################
            if struct.calcsize("P") == 4:
                self.ReturnA = numpy.zeros(self.ctmap.shape, int)
                CT.CTChange(self.ctmap, self.ReturnA, self.RS, self.RI)
                self.ctmap  = self.ReturnA
                del self.ReturnA
            elif struct.calcsize("P") == 8:
                self.ctmap = self.RS * self.ctmap + self.RI
                self.ctmap[numpy.where(self.ctmap < -1024)] = -1024
            #######################################################################################

            # CT is loaded add CT Loaded box
            if self.dsCT.PatientPosition == 'HFP':
                self.proneorsurf = -1
            elif self.dsCT.PatientPosition == 'HFS':
                self.proneorsurf = 1
        else:
            return 'Not CT'
    def STRUCT_load(self):
        if self.struct_path == 'None':
            return 'No path'
        try:
            self.dsstruct = dicom.read_file(self.struct_path, force = True)
        except:
            return 'Not dicom'

        if self.dsstruct.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.3':
            self.pnamest = self.dsstruct.PatientName
        else:
            return 'Not STRUCT'

    def PLAN_load(self):
        if self.plan_path == 'None':
            return 'No path'
        try:
            self.dsplan = dicom.read_file(self.plan_path, force=True)
        except:
            return 'Not dicom'

        if self.dsplan.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.8':
            self.pnamepl = self.dsplan.PatientName
        else:
            return 'Not PLAN'

    def Execution(self):
        self.iso_Plan = self.dsplan.IonBeamSequence[0].IonControlPointSequence[0].IsocenterPosition
        self.iso_IPP_corrected = [self.iso_Plan[0] - self.position[0], self.iso_Plan[1] - self.position[1],
                                  self.iso_Plan[2] - self.position[2]]
        self.ROI_z_grid_min = self.ctmap.shape[0]
        self.ROI_y_grid_min = self.ctmap.shape[1]
        self.ROI_x_grid_min = self.ctmap.shape[2]

        self.ROI_z_min = self.ctmap.shape[0] * self.PixelSpacing[2]
        self.ROI_y_min = self.ctmap.shape[1] * self.PixelSpacing[1]
        self.ROI_x_min = self.ctmap.shape[2] * self.PixelSpacing[0]