import os
import dicom
import numpy
import math
######################################################################################################################
# Fluka Input file functions
######################################################################################################################

##############################################################
# Calculating total particle per Each beam
#
# i is the loop for each beam sequences
##############################################################
def spotWeightcal(ds,i):
        
    #Defining variables
    spotWeights = []
    SumspotWeights = 0
    
    for h in range(0,len(ds.IonBeamSequence[i].IonControlPointSequence),2):
        # code for Raystation System
        
        if ds.Manufacturer == 'RaySearch Laboratories':
            for k in range(len(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)):
                if ds.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions==1:
                    if not ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights == 0:
                        spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)
                else:
                    if not ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k] == 0:
                        spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k])
                    
        # Typical Use of TPS for Nominal Beam Energy
        
        else:
            for k in range(ds.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions):
                if ds.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions==1:
                    spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)
                else:
                    spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k])
    # Sum whole spotWeights
    for j in range(len(spotWeights)):
        SumspotWeights = round(SumspotWeights +  spotWeights[j],1)

    # Min and Max values of spotWeights
    minE = round(min(spotWeights),1)
    maxE = round(max(spotWeights),1)
                
    return minE, maxE, SumspotWeights

##############################################################
# Calculating total subparticle per Each beam
#
# i is the loop for each beam sequences
# h is the loop for Ion control Point sequence
##############################################################
def spotsubWeights(i,h,ds):
    
    #Defining variables
    spotWeights = []
    SumspotWeights = 0
    
    # code for Raystation System
    if ds.Manufacturer == 'RaySearch Laboratories':
        for k in range(len(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)):
            if ds.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions==1:
                if not ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights == 0:
                    spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)
            else:
                if not ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k] == 0:
                    spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k])

    # Typical Use of TPS for Nominal Beam Energy
    else:
        for k in range(ds.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions):
            if ds.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions==1:
                spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)
            else:
                spotWeights.append(ds.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k])

    # Sum whole spotWeights
    for j in range(len(spotWeights)):
        SumspotWeights = round(SumspotWeights +  spotWeights[j],1)

    # Min and Max values of spotWeights
    minE = round(min(spotWeights),1)
    maxE = round(max(spotWeights),1)
                
    return minE, maxE, SumspotWeights

##############################################################
# FLUKA file creation
#
# beam4fluka_filenumber.txt
# transf4fluka_filenumber.txt
# are created
##############################################################
def dcm4fluka(dsplan, ctsize, prstag, path):
    noBeams = dsplan.FractionGroupSequence[0].NumberOfBeams

    # Save Path for Created Files. /Files/ must be the subdirectory for all created files.
    cpath = os.path.dirname(os.path.abspath( __file__ ))
    savePath = path
    # Directory Creation if not exist.
    if not os.path.exists(savePath):
        os.makedirs(savePath) 

    # i stand for the numbers of beam starting from 0

    # Reading Scan spot postion and calculate step size for Raysearch
    breaktage = False
    if dsplan.Manufacturer ==  'RaySearch Laboratories':
        for i in range(noBeams):
            for h in range(0, len(dsplan.IonBeamSequence[i].IonControlPointSequence), 2):
                if len(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights) > 2:
                    x_step_size = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[4] - dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[2]
                    y_step_size = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[5] - dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[3]
                    break
                    breaktage = True
            if breaktage:
                break


    for i in range(noBeams):
        # File writing
        filename = savePath+"beam4fluka_"+str(i+1)+".txt"
        f = open(filename, 'w')
        f.write("patient_id "+dsplan.PatientID+"\n")
        f.write("machine# 0\n")
        # Proton Case
        if dsplan.IonBeamSequence[0].RadiationType == 'PROTON':
            f.write("projectile 1H\n")
            f.write("charge 1\n")
            f.write("mass 1\n")
        # Carbon Case
        elif dsplan.IonBeamSequence[0].RadiationType == 'ION':
            f.write("projectile 12C\n")
            f.write("charge 6\n")
            f.write("mass 12\n")
        f.write("bolus "+str(dsplan.IonBeamSequence[0].NumberofBoli)+"\n")
        f.write("ripplefilter 0\n")
        f.write("#submachines "+str(int(round(len(dsplan.IonBeamSequence[i].IonControlPointSequence)/2)))+"\n")
        f.write("#particles " + str(spotWeightcal(dsplan,i)[0])+" "+str(spotWeightcal(dsplan,i)[1])+ " "+str(spotWeightcal(dsplan,i)[2])+"\n")
        # Global code for each txt end here
           
        spots = 0
        nomBeamEnergies = []
        spotWeights = []
        spotPosX = []
        spotPosY = [] 
        for h in range(0,len(dsplan.IonBeamSequence[i].IonControlPointSequence),2):
            # code for Raystation System
            if dsplan.Manufacturer == 'RaySearch Laboratories':

                # Gather information from the Private Tag while RaySearch file does not provide NominalBeamEnergy
                NBEMeV = float(dsplan.IonBeamSequence[i].IonControlPointSequence[h][0x40011008].value)
                MeVu2MeV = float(dsplan.IonBeamSequence[i].IonControlPointSequence[h][0x40011009].value)
                
                # Calculate the NominalBeamEnergy
                nomBE = NBEMeV/MeVu2MeV

                # Adding NominalBeamEnergy to the array with certain format
                nomBeamEnergies.append("%3.2f" %(nomBE))
                
                f.write("submachine# 0 "+" "+str(nomBE)+" 0 0 \n")
                f.write("#particles   "+str(spotsubWeights(i,h,dsplan)[0])+" "+str(spotsubWeights(i,h,dsplan)[1])+" "+str(spotsubWeights(i,h,dsplan)[2])+"\n")
                f.write("stepsize "+'{:{width}.{pref}f}'.format(x_step_size, width = 3, pref = 2) + " " + '{:{width}.{pref}f}'.format(y_step_size, width = 3, pref = 2)+"\n")
                f.write("#points "+str(dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions - 1)+"\n")
                # Case if ScanSpotMetersetWeights is float that cannot treated as array
                for k in range(len(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights)):
                    
                    # Case if ScanSpotMetersetWeights is float that cannot treated as array
                    if dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions == 1:
                        if not dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights == 0.0:
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]))
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]))
                            f.write("%3.1f\n" %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights))

                    # Case if ScanSpotMetersetWeights is array
                    else:
                        if not dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k] == 0.0:
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]))
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]))
                            f.write("%3.1f\n" %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]))
                        
            # Typical Use of TPS for Nominal Beam Energy
            else:
                f.write("submachine# 0 "+" "+str(dsplan.IonBeamSequence[i].IonControlPointSequence[h].NominalBeamEnergy)+" 0 0\n")
                f.write("#particles   "+str(spotsubWeights(i,h,dsplan)[0])+" "+str(spotsubWeights(i,h,dsplan)[1])+" "+str(spotsubWeights(i,h,dsplan)[2])+"\n")
                f.write("stepsize "+str(dsplan.IonBeamSequence[i][0x300b1085][0]*1.0)+" "+str(dsplan.IonBeamSequence[i][0x300b1085][1]*1.0)+"\n")
                f.write("#points "+str(dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions)+"\n")

                # Case if ScanSpotMetersetWeights is float that cannot treated as array
                for k in range(dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions):

                    # Case if ScanSpotMetersetWeights is float that cannot treated as array
                    if dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions == 1:
                        if not dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights == 0.0:
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]))
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]))
                            f.write("%3.1f\n" %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights))

                    # Case if ScanSpotMetersetWeights is array
                    else:
                        if not dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k] == 0.0:
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]))
                            f.write("%3.2f " %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]))
                            f.write("%3.1f\n" %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]))

                        
 
                #Get X and Y position
                for j in range(len(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap)):
                    if j % 2 == 0:
                        spotPosX.append("%3.2f" %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[j]))
                    else:
                        spotPosY.append("%3.2f" %(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[j]))

        f.close()
        tfulka = savePath + "transf4fluka_"+str(i+1)+".txt"
        ft = open(tfulka, 'w')
        gA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].GantryAngle)
        PSA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].PatientSupportAngle)
        TTPA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].TableTopPitchAngle)
        TTRA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].TableTopRollAngle)
        ISOP = dsplan.IonBeamSequence[i].IonControlPointSequence[0].IsocenterPosition
        if prstag == 1:
            ft.write("%4.1f %4.1f %4.2f %4.2f\n"%(gA,PSA,TTPA,TTRA))
            ft.write("%10.5f %10.5f %10.5f\n"%(ISOP[0]/10,ISOP[1]/10,ISOP[2]/10))
        elif prstag == - 1:
            if TTRA > 0:
                ft.write("%4.1f %4.1f %4.2f %4.2f\n"%(gA,PSA,TTPA,TTRA - 180))
            else:
                ft.write("%4.1f %4.1f %4.2f %4.2f\n"%(gA,PSA,TTPA,TTRA + 180))
            ft.write("%10.5f %10.5f %10.5f\n"%(ISOP[0]/10 + ctsize[0]/10,ISOP[1]/10 + ctsize[1]/10,ISOP[2]/10))
        ft.write("  -0.00000   -0.00000   -0.00000")
        ft.close
        Wat2Thick = 1.048
        if not dsplan.IonBeamSequence[i].NumberofRangeShifters == 0:
            RSfluka = savePath + "RS_"+ str(i+1)+".txt"
            ftt = open(RSfluka, 'w')
            ftt.write('*   RANGE SHIFTER\n')
            if dsplan.Manufacturer == 'RaySearch Laboratories':
                RSdist = dsplan.IonBeamSequence[i].IonControlPointSequence[0].SnoutPosition / 10 * -1
                RSWEThick = 3
                Wat2Thick = 1
            else:
                RSdist = dsplan.IonBeamSequence[i].IonControlPointSequence[0].RangeShifterSettingsSequence[0].IsocenterToRangeShifterDistance /10 * -1
                RSWEThick = dsplan.IonBeamSequence[i].IonControlPointSequence[0].RangeShifterSettingsSequence[0].RangeShifterWaterEquivalentThickness/10
            ftt.write("RPP RSHIFT     -15. 15. -15. 15. %2.6f %2.6f\n"%(RSdist, RSdist + RSWEThick/Wat2Thick))
            ftt.close()

##############################################################
# FLUKA file creation with Ray Tracing
#
# beam4fluka_filenumber.txt
# transf4fluka_filenumber.txt
# are created
##############################################################
def RTdcm4fluka(dsplan, Points, NoBeamlist):
    noBeams = dsplan.FractionGroupSequence[0].NumberOfBeams

    # Save Path for Created Files. /Files/ must be the subdirectory for all created files.
    cpath = os.path.dirname(os.path.abspath( __file__ ))
    savePath = cpath + '/RayTraceFiles/'
    # Directory Creation if not exist.
    if not os.path.exists(savePath):
        os.makedirs(savePath) 

    # i stand for the numbers of beam starting from 0
    for i in range(noBeams):
        # File writing
        filename = savePath+"beam4fluka_"+str(i+1)+".txt"
        f = open(filename, 'w')
        f.write("patient_id "+dsplan.PatientID+"\n")
        f.write("machine# 0\n")
        # Proton Case
        if dsplan.IonBeamSequence[0].RadiationType == 'PROTON':
            f.write("projectile 1H\n")
            f.write("charge 1\n")
            f.write("mass 1\n")
        # Carbon Case
        elif dsplan.IonBeamSequence[0].RadiationType == 'ION':
            f.write("projectile 12C\n")
            f.write("charge 6\n")
            f.write("mass 12\n")
        f.write("bolus "+str(dsplan.IonBeamSequence[0].NumberofBoli)+"\n")
        f.write("ripplefilter 0\n")
        f.write("#submachines "+str(round(len(dsplan.IonBeamSequence[i].IonControlPointSequence)/2))+"\n")
        f.write("#particles " + str(spotWeightcal(dsplan,i)[0])+" "+str(spotWeightcal(dsplan,i)[1])+ " "+str(spotWeightcal(dsplan,i)[2])+"\n")
        # Global code for each txt end here
           
        spots = 0
        nomBeamEnergies = []
        spotWeights = []
        spotPosX = []
        spotPosY = [] 
        # code for Raystation System
        if dsplan.Manufacturer == 'RaySearch Laboratories':

            # Gather information from the Private Tag while RaySearch file does not provide NominalBeamEnergy
            for BeamN in range(0, len(NoBeamlist)):
                    

                # Adding NominalBeamEnergy to the array with certain format
                nomBeamEnergies.append("%3.2f" %(NoBeamlist[BeamN]))
                
                f.write("submachine# 0 "+" "+str(NoBeamlist[BeamN])+" 0 0 \n")
                f.write("#particles   1.0 1.0 1.0\n")
                    
                for pos in range(0, len(Points[BeamN])):
                    
                    f.write("%3.2f " %(Points[BeamN][pos][0]))
                    f.write("%3.2f " %(Points[BeamN][pos][1]))
                    f.write("1\n")
    
                    spotPosX.append("%3.2f" %(Points[BeamN][pos][0]))
                    spotPosY.append("%3.2f" %(Points[BeamN][pos][1]))
        else:
         
            # Gather information from the Private Tag while RaySearch file does not provide NominalBeamEnergy
            for BeamN in range(0, len(NoBeamlist)):
                f.write("submachine# 0 "+" "+str(NoBeamlist[BeamN])+" 0 0\n")
                f.write("#particles   "+str(1.0)+" "+str(1.0)+" "+str(1.0)+"\n")
                f.write("stepsize "+str(dsplan.IonBeamSequence[i][0x300b1085][0]*1.0)+" "+str(dsplan.IonBeamSequence[i][0x300b1085][1]*1.0)+"\n")
                f.write("#points "+str(len(Points[BeamN]))+"\n")   

                # Adding NominalBeamEnergy to the array with certain format
                nomBeamEnergies.append("%3.2f" %(NoBeamlist[BeamN]))
                    

                for pos in range(0, len(Points[BeamN])):
                    f.write("%3.2f " %(Points[BeamN][pos][0]))
                    f.write("%3.2f " %(Points[BeamN][pos][1]))
                    f.write("1\n")
    
                    spotPosX.append("%3.2f" %(Points[BeamN][pos][0]))
                    spotPosY.append("%3.2f" %(Points[BeamN][pos][1]))
                    
        f.close()
        tfulka = savePath + "transf4fluka_"+str(i+1)+".txt"
        ft = open(tfulka, 'w')
        gA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].GantryAngle)
        PSA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].PatientSupportAngle)
        TTPA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].TableTopPitchAngle)
        TTRA = int(dsplan.IonBeamSequence[i].IonControlPointSequence[0].TableTopRollAngle)
        ISOP = dsplan.IonBeamSequence[i].IonControlPointSequence[0].IsocenterPosition
        ft.write("%4.1f %4.1f %4.2f %4.2f\n"%(gA,PSA,TTPA,TTRA))
        ft.write("%10.5f %10.5f %10.5f\n"%(ISOP[0]/10,ISOP[1]/10,ISOP[2]/10))
        ft.write("  -0.00000   -0.00000   -0.00000")
        ft.close
        
        if not dsplan.IonBeamSequence[i].NumberofRangeShifters == 0:
            RSfluka = savePath + "RS_"+ str(i+1)+".txt"
            ftt = open(RSfluka, 'w')
            ftt.write('*   RANGE SHIFTER\n')
            RSdist = dsplan.IonBeamSequence[i].IonControlPointSequence[0].RangeShifterSettingsSequence[0].IsocenterToRangeShifterDistance /10 * -1
            RSWEThick = dsplan.IonBeamSequence[i].IonControlPointSequence[0].RangeShifterSettingsSequence[0].RangeShifterWaterEquivalentThickness/10
            ftt.write("RPP RSHIFT     -15. 15. -15. 15. %2.6f %2.6f\n"%(RSdist, RSdist + RSWEThick/Wat2Thick))
            ftt.close()

##############################################################
# 
#
# DICOM PLANS
# 
# 
##############################################################
def scanspotgen(dsplan,nsubpb,vacuum_len_0,FWHM_len_iso,FWHMALL):
    noBeams = dsplan.FractionGroupSequence[0].NumberOfBeams
    # i stand for the numbers of beam starting from 0
    S2FWHM = 2.0*math.sqrt(2.*math.log(2.0))
    tot_spot = []
    for i in range(noBeams):
        spots = 0
        nomBeamEnergies = []
        spotWeights = []
        spotPosX = []
        spotPosY = []
        infodic = {}
        temp_isoc = dsplan.IonBeamSequence[i].IonControlPointSequence[0].IsocenterPosition
        temp_skin = dsplan.IonBeamSequence[i].IonControlPointSequence[0].SurfaceEntryPoint
        dist_iso_skin = ((temp_isoc[0] - temp_skin[0]) ** 2) + ((temp_isoc[1] - temp_skin[1]) ** 2) + ((temp_isoc[2] - temp_skin[2]) ** 2)
        fout = open('FWHM_out.txt','w')
        vacuum_len = vacuum_len_0 - math.sqrt(dist_iso_skin)	
        for h in range(0,len(dsplan.IonBeamSequence[i].IonControlPointSequence),2):          
            # Typical Use of TPS for Nominal Beam Energy
            submachine = str(dsplan.IonBeamSequence[i].IonControlPointSequence[h].NominalBeamEnergy*100)
            fout.write('E: '+str(submachine)+ ' ')
            fout.write('FWHMiso: '+str(FWHMALL[submachine])+' ')
            FWHM= [FWHM_len_iso, FWHMALL[submachine]]
            FWHMPB = (numpy.interp(vacuum_len, FWHM[0], FWHM[1]))
            fout.write('FWHMskin: '+str(FWHMPB)+'\n\n')
            dataset = []
            fluence_FAC=1
            for k in range(dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions):      
                # Case if ScanSpotMetersetWeights is float that cannot treated as array
                if dsplan.IonBeamSequence[i].IonControlPointSequence[h].NumberOfScanSpotPositions == 1:
                    if nsubpb == 1:
                        dataset.append([dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2], dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1], fluence_FAC*(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights), FWHMPB])
                    elif nsubpb == 4:
                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[(k*2)+1]
                        fcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights
                        scenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotTuneID
                        #(0,1)
                        xxsubpb = xcenter
                        yysubpb = ycenter+FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(0,-1)
                        xxsubpb = xcenter
                        yysubpb = ycenter-FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(1,0)
                        xxsubpb = xcenter+FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,0)
                        xxsubpb = xcenter-FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter
                        ffsubpb = fcenter/4.0
                        fwhmsub = math.sqrt(3)*FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                    elif nsubpb == 9:
                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[(k*2)+1]
                        fcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights
                        scenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotTuneID
                        #
                        # missing sigma center TRUE, should be sampled in AIR not the one at iso!
                        #
                        #subsplitting in 9
                        #(0,0)
                        xxsubpb = xcenter
                        yysubpb = ycenter
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                
                        #(0,1)
                        xxsubpb = xcenter
                        yysubpb = ycenter+FWHMPB/S2FWHM
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(0,-1)
                        xxsubpb = xcenter
                        yysubpb = ycenter-FWHMPB/S2FWHM
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(1,0)
                        xxsubpb = xcenter+FWHMPB/S2FWHM
                        yysubpb = ycenter
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,0)
                        xxsubpb = xcenter-FWHMPB/S2FWHM
                        yysubpb = ycenter
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,-1)
                        xxsubpb = xcenter-FWHMPB/S2FWHM
                        yysubpb = ycenter-FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(1,1)
                        xxsubpb = xcenter+FWHMPB/S2FWHM
                        yysubpb = ycenter+FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                    
                        #(1,-1)
                        xxsubpb = xcenter+FWHMPB/S2FWHM
                        yysubpb = ycenter-FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,1)
                        xxsubpb = xcenter-FWHMPB/S2FWHM
                        yysubpb = ycenter+FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub]) 
                    elif nsubpb == 16:

                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]
                        fcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights
                        # missing sigma center TRUE, should be sampled in AIR not the one at iso!!!!!!!!!!!
                        #
                        #subsplitting in 16

                        ############################
                        #(+1/2,-1/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(+1/2,+1/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(-1/2,-1/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                

                        #(-1/2,+1/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

                        ############################

                        #(-3/2,-1/2)
                        xxsubpb = xcenter - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(-3/2,+1/2)
                        xxsubpb = xcenter - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(+3/2,-1/2)
                        xxsubpb = xcenter + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])         
                        #(+3/2,+1/2)
                        xxsubpb = xcenter + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

       
                        #(-1/2,+3/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

                        #(-1/2,-3/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

                        #(+1/2,+3/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
     
                        #(+1/2,-3/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                


                        ############################

                        #(-3/2,-3/2)
                        xxsubpb = xcenter  - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                       #(-3/2,+3/2)
                        xxsubpb = xcenter  - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(+3/2,-3/2)
                        xxsubpb = xcenter  + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                
                        #(+3/2,+3/2)
                        xxsubpb = xcenter  + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])     

                        ############################

                    elif nsubpb == 64:
                        #print('SPLITTING 64')

                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]
                        fcenter =fluence_FAC* dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights
                        # missing sigma center TRUE, should be sampled in AIR not the one at iso!!!!!!!!!!!
                        #
                        #subsplitting in 16
                        ############################
                        #(+1/2,-1/2)
                        for split4 in range(0,16):
                            if split4==0:
                                #(+1/2,-1/2)
                                xxPB = xcenter + FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==1:
                                #(-1/2,-1/2)
                                xxPB = xcenter-FWHMPB/(2*S2FWHM)
                                yyPB = ycenter-FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==2:
                                #(+1/2,+1/2)
                                xxPB = xcenter+FWHMPB/(2*S2FWHM)
                                yyPB = ycenter+FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==3:
                                #(-1/2,+1/2)
                                xxPB = xcenter-FWHMPB/(2*S2FWHM)
                                yyPB = ycenter+FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==4:
                            #(-3/2,-1/2)    
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==5:
                            #(-3/2,+1/2)
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==6:
                            #(+3/2,-1/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==7:
                            #(+3/2,+1/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==8:
                            #(-1/2,+3/2)
                                xxPB = xcenter - FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==9:
                            #(-1/2,-3/2)
                                xxPB = xcenter - FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==10:
                            #(+1/2,+3/2)
                                xxPB = xcenter + FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==11:
                            #(+1/2,-3/2)
                                xxPB = xcenter + FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==12:
                        #(-3/2,-3/2)
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==13:
                        #(-3/2,+3/2)
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==14:
                            #(+3/2,-3/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==15:
                            #(+3/2,+3/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2

                            for sub_split4 in range(0,4):
                                if sub_split4==0:
                                    #(+1/2,-1/2)
                                    xxsubpb = xxPB+fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB+fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                                elif sub_split4==1:
                                    #(-1/2,-1/2)
                                    xxsubpb = xxPB-fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB-fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                                elif sub_split4==2:
                                    #(+1/2,-1/2)
                                    xxsubpb = xxPB+fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB-fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                                elif sub_split4==3:
                                    #(-1/2,+1/2)
                                    xxsubpb = xxPB-fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB+fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                       
                # Case if ScanSpotMetersetWeights is array
                else:
                    if nsubpb == 1:
                        dataset.append([dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2], dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1],  fluence_FAC*(dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]), FWHMPB])
                    elif nsubpb == 4:    
                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]
                        fcenter =fluence_FAC* dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]
                        #(0,1)
                        xxsubpb = xcenter
                        yysubpb = ycenter+FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(0,-1)
                        xxsubpb = xcenter
                        yysubpb = ycenter-FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(1,0)
                        xxsubpb = xcenter+FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,0)
                        xxsubpb = xcenter-FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter
                        ffsubpb = fcenter/4.0
                        fwhmsub = math.sqrt(3)*FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                    elif nsubpb == 9:
                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]
                        fcenter =fluence_FAC* dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]
                        # missing sigma center TRUE, should be sampled in AIR not the one at iso!!!!!!!!!!!
                        #
                        #subsplitting in 9
                        #(0,0)
                        xxsubpb = xcenter
                        yysubpb = ycenter
                        ffsubpb = fcenter/4.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                
                        #(0,1)
                        xxsubpb = xcenter
                        yysubpb = ycenter+FWHMPB/S2FWHM
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(0,-1)
                        xxsubpb = xcenter
                        yysubpb = ycenter-FWHMPB/S2FWHM
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(1,0)
                        xxsubpb = xcenter+FWHMPB/S2FWHM
                        yysubpb = ycenter
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,0)
                        xxsubpb = xcenter-FWHMPB/S2FWHM
                        yysubpb = ycenter
                        ffsubpb = fcenter/8.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,-1)
                        xxsubpb = xcenter-FWHMPB/S2FWHM
                        yysubpb = ycenter-FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(1,1)
                        xxsubpb = xcenter+FWHMPB/S2FWHM
                        yysubpb = ycenter+FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                    
                        #(1,-1)
                        xxsubpb = xcenter+FWHMPB/S2FWHM
                        yysubpb = ycenter-FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                        #(-1,1)
                        xxsubpb = xcenter-FWHMPB/S2FWHM
                        yysubpb = ycenter+FWHMPB/S2FWHM
                        ffsubpb = fcenter/16.0
                        fwhmsub = FWHMPB/math.sqrt(2)
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])
                    elif nsubpb == 16:
                        #print('SPLITTING 16')
                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]
                        fcenter =fluence_FAC* dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]
                        # missing sigma center TRUE, should be sampled in AIR not the one at iso!!!!!!!!!!!
                        #
                        #subsplitting in 16
                        ############################
                        #(+1/2,-1/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(+1/2,+1/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(-1/2,-1/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                

                        #(-1/2,+1/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb = 9*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

                        ############################
                        #SUBSPLIT 4 main by n=4
                        ############################

                        #(-3/2,-1/2)
                        xxsubpb = xcenter - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(-3/2,+1/2)
                        xxsubpb = xcenter - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(+3/2,-1/2)
                        xxsubpb = xcenter + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])         
                        #(+3/2,+1/2)
                        xxsubpb = xcenter + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + FWHMPB/(2*S2FWHM)
                        ffsubpb =3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  
       
                        #(-1/2,+3/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

                        #(-1/2,-3/2)
                        xxsubpb = xcenter - FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])  

                        #(+1/2,+3/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
     
                        #(+1/2,-3/2)
                        xxsubpb = xcenter + FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = 3*fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                

                        ############################
                        #(-3/2,-3/2)
                        xxsubpb = xcenter  - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                       #(-3/2,+3/2)
                        xxsubpb = xcenter  - 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])       
                        #(+3/2,-3/2)
                        xxsubpb = xcenter  + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  - 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])                
                        #(+3/2,+3/2)
                        xxsubpb = xcenter  + 3*FWHMPB/(2*S2FWHM)
                        yysubpb = ycenter  + 3*FWHMPB/(2*S2FWHM)
                        ffsubpb = fcenter/64
                        fwhmsub = FWHMPB/2
                        dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])     
                        ###########################
                    elif nsubpb == 64:
                        #print('SPLITTING 28')
                        xcenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2]
                        ycenter = dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotPositionMap[k*2+1]
                        fcenter =fluence_FAC* dsplan.IonBeamSequence[i].IonControlPointSequence[h].ScanSpotMetersetWeights[k]
                        # missing sigma center TRUE, should be sampled in AIR not the one at iso!!!!!!!!!!!
                        #
                        #subsplitting in 16
						############################
                        #(+1/2,-1/2)
                        for split4 in range(0,16):
                            if split4==0:
                                #(+1/2,-1/2)
                                xxPB = xcenter + FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==1:
                                #(-1/2,-1/2)
                                xxPB = xcenter-FWHMPB/(2*S2FWHM)
                                yyPB = ycenter-FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==2:
                                #(+1/2,+1/2)
                                xxPB = xcenter+FWHMPB/(2*S2FWHM)
                                yyPB = ycenter+FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==3:
                                #(-1/2,+1/2)
                                xxPB = xcenter-FWHMPB/(2*S2FWHM)
                                yyPB = ycenter+FWHMPB/(2*S2FWHM)
                                ffPB4 = 9*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==4:
                            #(-3/2,-1/2)    
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==5:
                            #(-3/2,+1/2)
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==6:
                            #(+3/2,-1/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==7:
                            #(+3/2,+1/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==8:
                            #(-1/2,+3/2)
                                xxPB = xcenter - FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==9:
                            #(-1/2,-3/2)
                                xxPB = xcenter - FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==10:
                            #(+1/2,+3/2)
                                xxPB = xcenter + FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==11:
                            #(+1/2,-3/2)
                                xxPB = xcenter + FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = 3*fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==12:
                        #(-3/2,-3/2)
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==13:
                        #(-3/2,+3/2)
                                xxPB = xcenter - 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==14:
                            #(+3/2,-3/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter - 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2
                            elif split4==15:
                            #(+3/2,+3/2)
                                xxPB = xcenter + 3*FWHMPB/(2*S2FWHM)
                                yyPB = ycenter + 3*FWHMPB/(2*S2FWHM)
                                ffPB4 = fcenter/64
                                fwhmPB = FWHMPB/2

                            for sub_split4 in range(0,4):
                                if sub_split4==0:
                                    #(+1/2,-1/2)
                                    xxsubpb = xxPB+fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB+fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                                elif sub_split4==1:
                                    #(-1/2,-1/2)
                                    xxsubpb = xxPB-fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB-fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                                elif sub_split4==2:
                                    #(+1/2,-1/2)
                                    xxsubpb = xxPB+fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB-fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                                elif sub_split4==3:
                                    #(-1/2,+1/2)
                                    xxsubpb = xxPB-fwhmPB/S2FWHM/2.0
                                    yysubpb = yyPB+fwhmPB/S2FWHM/2.0
                                    ffsubpb = ffPB4/4.0
                                    fwhmsub = fwhmPB*math.sqrt(3)/2.
                                    dataset.append([xxsubpb,yysubpb,ffsubpb,fwhmsub])   
                        
                #k=1                  
            infodic[submachine] = dataset
        tot_spot.append(infodic)
        fout.close()    
    return tot_spot


###############################################################

# XML PLANS

###############################################################
def scanspotgenXML(XML_data,nsubpb,vacuum_len_0,FWHM_len_iso,FWHMALL):
    noBeams =1
    S2FWHM = 2.0*math.sqrt(2.*math.log(2.0))
    tot_spot = []
    for i in range(noBeams):
        spots = 0
        nomBeamEnergies = []
        spotWeights = []
        spotPosX = []
        spotPosY = []
        infodic = {}
        temp_isoc = [573.5, -339.5, -265] # ????????????????
        temp_skin = [573.5, -339.5, -265] # ????????????????  
        dist_iso_skin=0        
        fout = open('FWHM_out.txt','w')
        vacuum_len = 1126 # HIT CONSTANT?
        keys_IES = XML_data[0].keys()	
        for h in range(0,len(keys_IES)):            
            submachine = keys_IES[h]
            print('\n'+str(submachine))
            FWHM= [FWHM_len_iso, FWHMALL[submachine]]
            FWHMPB  = (numpy.interp(vacuum_len, FWHM[0], FWHM[1]))
            dataset = []
            IES = XML_data[0][submachine]
            print('NUM SPOTS: '+str(len(IES)))
            fluence_FAC=1
            for k in range(0,len(IES)):
                xcenter = float(IES[k][0])
                ycenter = float(IES[k][1])
                fcenter = float(IES[k][2]) #ScanSpotMetersetWeights
                # Case if ScanSpotMetersetWeights is float that cannot treated as array
                if nsubpb == 1:
                    dataset.append([xcenter,ycenter,fcenter,FWHMPB])
            infodic[submachine] = dataset
        tot_spot.append(infodic)
        fout.close()
    return tot_spot
