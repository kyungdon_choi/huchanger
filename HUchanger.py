# TOLIST

# Prone or surfine
# beam4fluka.txt generation
# CT matrix txt output
# HU changer

# Python built-in functions
import sys
import os
import platform
import math
import shutil
from os.path import join as pjoin
from decimal import Decimal

# PyQt4 import
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import PyQt4.Qt
from PyQt4 import uic
from PyQt4.QtGui import QImage

# Matplotlib and numpy
import numpy
import numpy.ma as ma

# Other module import
import qimage2ndarray
import dicom
import shapely
from shapely.geometry import Polygon
import shapely.speedups

# Built functions
import ReadDicom as RD
import StructSet as SS
import FlukaGeneration as FG
import struct
if struct.calcsize("P") == 4:
    import Contours as CT
elif struct.calcsize("P") == 8:
    import Contours_64bit as CT
import Contour_class as ConClass

import time

# UI load form
form_class = uic.loadUiType('Unified_GUI.ui')[0]
bar_class = uic.loadUiType('Bar.ui')[0]
# CT renorm for WEPL calculation
def RenormCT(CTList, Array): # CTList is the array that stores CT path and name. Array is the CT dicom array.
    ReturnA = numpy.zeros(Array.shape, int)
    ds = dicom.read_file(CTList[0])
    RI = ds.RescaleIntercept
    RS = ds.RescaleSlope
    if struct.calcsize("P") == 4:
        CT.CTChange(Array, ReturnA, RS, RI)
    elif struct.calcsize("P") == 8:
        ReturnA = RS * Array + RI
        ReturnA[numpy.where(ReturnA < -1024)] = -1024

    return ReturnA

# Change HU of CT
def contourchange(Xarray, Yarray, ctmap, zpos, pixd, vals):
    Pointsets = []
    tpoints = []
    if len(Xarray) == 0:
        return
    else:

        for i in range(0, len(Xarray)):
            tpoints.append(Xarray[i])
            tpoints.append(Yarray[i])
            Pointsets.append(tpoints)
            tpoints = []
        poly = Polygon(Pointsets)
        polydatt = poly.bounds
        polydat = list(polydatt)
        rpolydat = []
        for i in range(0, len(polydat)):
            if i == 0 or i == 1:
                rpolydat.append(int(polydat[i]/pixd[i]))
            else:
                rpolydat.append(int(polydat[i]/pixd[i-2]))
        for j in range(rpolydat[1], rpolydat[3], 1):
            for i in range(rpolydat[0], rpolydat[2], 1):
                testpoint = shapely.geometry.Point(i*pixd[0],j*pixd[1])
                if poly.contains(testpoint):
                    ctmap[zpos,j,i] = vals

class Mainwindow(QMainWindow, form_class):
    def __init__(self):
        super(Mainwindow, self).__init__()
        self.setupUi(self)
        self.connect(self.CTLoad, SIGNAL("clicked()"), self.ct_clicked)
        self.connect(self.STLoad, SIGNAL("clicked()"), self.st_clicked)
        self.connect(self.PLLoad, SIGNAL("clicked()"), self.pl_clicked)
        self.connect(self.EXEC, SIGNAL("clicked()"), self.exec_clicked)
        self.connect(self.HUC, SIGNAL("clicked()"), self.HUCH_clicked)
        self.connect(self.CTout, SIGNAL("clicked()"), self.ctout_clicked)
        self.connect(self.FLUKAGen, SIGNAL("clicked()"), self.flgen_clicked)
        self.connect(self.UNDO, SIGNAL("clicked()"), self.undo_clicked)
        self.connect(self.ROIout, SIGNAL("clicked()"), self.roiout_clicked)
        self.connect(self.Raster_gen, SIGNAL("clicked()"), self.Raster_clicked)
        self.CTSlider.valueChanged.connect(self.valchange)
        self.ThreadSlide.valueChanged.connect(self.core_display)
        self.ThreadSlide.setMinimum(1)
        self.ThreadSlide.setMaximum(40)

        self.contour_obj = ConClass.contour_part()
        self.cpath = os.path.dirname(os.path.abspath( __file__ ))
        self.savePath = self.cpath + '/Files/'
        self.ROI_x_min = 0.0
        self.ROI_x_max = 0.0
        self.ROI_y_min = 0.0
        self.ROI_y_max = 0.0
        self.ROI_z_min = 0.0
        self.ROI_z_max = 0.0

        self.ROI_x_grid_min = 0
        self.ROI_x_grid_max = 0
        self.ROI_y_grid_min = 0
        self.ROI_y_grid_max = 0
        self.ROI_z_grid_min = 0
        self.ROI_z_grid_max = 0

        # Set Scene
        self.scene = QGraphicsScene()
        self.dvhviewer = QGraphicsView()
        self.setWindowTitle("HU Changer")
        
    # CT Load clicked
    def ct_clicked(self):
        self.openct = str(QFileDialog.getOpenFileName(self)) # Load CT path with file browser

        # Check if it is a dicom file or not and generate a flag to run if it is true
        try:
            self.dsCT = dicom.read_file(self.openct)
            self.flag = True
        except:
            QMessageBox.about(self, "Error Message", "This is not a dicom file")
            self.flag = False

        # If it is a dicom file
        if self.flag == True:

            # If it is a CT Image
            if self.dsCT.SOPClassUID == '1.2.840.10008.5.1.4.1.1.2':

                # Get patient name inside the CT file
                self.pnamect = self.dsCT.PatientName
                self.openct = self.openct.split('/') # split the path with / to reconstruct the CT file path
                self.path = ''
                self.filelist = []
                self.ctlist = []

                # Construct the path depend on the system
                if platform.system() == 'Windows':
                    for i in range(0, len(self.openct)-1):
                        self.path = self.path + self.openct[i] + '/'
                elif platform.system() == 'Linux':
                    self.path = '/'
                    for i in range(0, len(self.openct)-1):
                        self.path = self.path + self.openct[i] + '/'

                self.filelist = RD.dcmRead(str(self.path))     # Get All files inside the folder or directory
                self.ctlist = RD.readCTdcm(self.filelist)      # Find CT dicoms from the filelist 
                self.row = int(self.dsCT.Rows)                 # Read row size from CT
                self.column = int(self.dsCT.Columns)           # Read column size from CT
                self.height = len(self.ctlist)                 # Read height size from number of CT slices
                self.thickn = self.dsCT.SliceThickness         # Get the CT thickness
                self.position = self.dsCT.ImagePositionPatient # Get Image Position Patient for future use
                self.ctmap = numpy.zeros((self.height, self.row, self.column), numpy.int16)  # Construct a array for CT map
                self.minloc = 999999                           # Temp min location for the calculation
                self.PixelSpacing = [self.dsCT.PixelSpacing[0], self.dsCT.PixelSpacing[1], self.thickn] # Construct PixelSpacing array
                self.size = [self.ctmap.shape[2] * self.PixelSpacing[0], self.ctmap.shape[1] * self.PixelSpacing[1], self.ctmap.shape[0] * self.PixelSpacing[2]]
                #print(self.PixelSpacing)
                #self.PixelSpacing = [self.dsCT.PixelSpacing[0], self.thickn, self.dsCT.PixelSpacing[1]] # Construct PixelSpacing array
                #print(self.PixelSpacing)
                # Calculate minimum location to align CT image
                for i in self.ctlist:
                    self.dstemp = dicom.read_file(i)
                    self.tminloc = self.dstemp.SliceLocation
                    if self.tminloc < self.minloc:
                        self.minloc = self.tminloc
                self.position = [self.position[0], self.position[1], self.minloc]
                    # CT matrix stored
                for i in self.ctlist:
                    self.dstemp = dicom.read_file(i)
                    self.ctmap[int((self.dstemp.SliceLocation - self.minloc)/self.thickn) ,:,:] = self.dstemp.pixel_array

                self.ctmap = RenormCT(self.ctlist, self.ctmap)  # Hounsfield modified CT Image
                
                # CT is loaded add CT Loaded box
                self.proneorsurf = 1
                if self.dsCT.PatientPosition == 'HFP':
                    self.proneorsurf = -1
                elif self.dsCT.PatientPosition == 'HFS':
                    self.proneorsurf = 1
                if self.CTcheck.count() == 0:
                    self.item = QListWidgetItem('Loaded')
                    self.CTcheck.addItem(self.item)
                if self.Patient_Name_box.count() == 0:
                    self.item = QListWidgetItem(str(self.pnamect))
                    self.Patient_Name_box.addItem(self.item)
                elif self.Patient_Name_box.count() != 0:
                    self.item = QListWidgetItem(str(self.pnamect))
                    self.Patient_Name_box.clear()
                    self.Patient_Name_box.addItem(self.item)
    # Structure clicked
    def st_clicked(self):
        self.openst = str(QFileDialog.getOpenFileName(self))
        try:
            self.dsstruct = dicom.read_file(self.openst, force = True)
            self.flag = True
        except:
            QMessageBox.about(self, "Error Message", "This is not a dicom file\nWrong Structure file")
            self.flag = False

        self.contour_obj.contour_list_get(self.dsstruct)
        self.contour_obj.Get_contouring_geo()

        if self.flag == True:
            if self.dsstruct.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.3':
                self.pnamest = self.dsstruct.PatientName
                if self.STcheck.count() == 0:
                    self.item = QListWidgetItem('Loaded')
                    self.STcheck.addItem(self.item)
                del self.flag
            else:
                QMessageBox.about(self, "Error Message", "Select RT Plan dicom file")
                
    # RT Plan clicked
    def pl_clicked(self):
        self.openpl = str(QFileDialog.getOpenFileName(self))
        try:
            self.dsplan = dicom.read_file(self.openpl)
            self.flag = True
        except:
            QMessageBox.about(self, "Error Message", "This is not a dicom file\nWrong Plan file")
            self.flag = False
            return

        if self.flag == True:
            if self.dsplan.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.8':
                if self.Plan_Name_box.count() == 0:
                    self.item = QListWidgetItem(str(self.dsplan.RTPlanName))
                    self.Plan_Name_box.addItem(self.item)
                self.pnamepl = self.dsplan.PatientName
                if self.PLcheck.count() == 0:
                    self.item = QListWidgetItem('Loaded')
                    self.PLcheck.addItem(self.item)
                    del self.flag
                if self.CTcheck.count() == 0:
                    QMessageBox.about(self, "CT requires", "Select any CT dicom file")
                    self.openct = str(QFileDialog.getOpenFileName(self)) # Load CT path with file browser
                    self.dsCT = dicom.read_file(self.openct)
                    self.row = int(self.dsCT.Rows)                 # Read row size from CT
                    self.column = int(self.dsCT.Columns)           # Read column size from CT
                    self.thickn = self.dsCT.SliceThickness         # Get the CT thickness
                    self.PixelSpacing = [self.dsCT.PixelSpacing[0], self.dsCT.PixelSpacing[1], self.thickn] # Construct PixelSpacing array
                    # CT is loaded add CT Loaded box
                    self.proneorsurf = 1
                    if self.dsCT.PatientPosition == 'HFP':
                        self.proneorsurf = -1
                    elif self.dsCT.PatientPosition == 'HFS':
                        self.proneorsurf = 1
                    self.contour_obj.Get_External_contour(self.dsstruct, self.proneorsurf, self.size, self.position)

            else:
                QMessageBox.about(self, "Error Message", "This is not a Plan Dicom file")
                
    # EXCUTION clicked
    def exec_clicked(self):
        if self.PLcheck.count() == 1:
            pass
        else:
            QMessageBox.about(self, "Error Message", "RT Plan file is not loaded")
            return()
        # Check if all files are loaded
        if self.CTcheck.count() == 1 and self.STcheck.count() == 1:
            # Check if all files patient name is same
            if  self.pnamect == self.pnamest:
                self.reply = 16384
            # If name is different ask if preceed or not.
            else:
                self.reply = QMessageBox.question(self, 'Message', "Patient names are different.\nKeep working?", QMessageBox.Yes, QMessageBox.No)

            # Preceed
            if self.reply == 16384:
                self.savePath = self.cpath + '/' + self.pnamect + '/'
                if not os.path.exists(self.savePath):
                    os.makedirs(self.savePath)
                    if self.dsplan.Manufacturer == 'RaySearch Laboratories':
                        f = open(self.savePath + 'Raysearch_File', 'w')
                        f.write('This plan is from Raystation.')
                        f.close()
                self.iso_Plan = self.dsplan.IonBeamSequence[0].IonControlPointSequence[0].IsocenterPosition
                print(self.iso_Plan)
                print(self.position)
                self.iso_IPP_corrected = [self.iso_Plan[0] - self.position[0], self.iso_Plan[1] - self.position[1], self.iso_Plan[2] - self.position[2]]
                print(self.iso_IPP_corrected)
                self.ROI_z_grid_min = self.ctmap.shape[0]
                self.ROI_y_grid_min = self.ctmap.shape[1]
                self.ROI_x_grid_min = self.ctmap.shape[2]

                self.ROI_z_min = self.ctmap.shape[0] * self.PixelSpacing[2]
                self.ROI_y_min = self.ctmap.shape[1] * self.PixelSpacing[1]
                self.ROI_x_min = self.ctmap.shape[2] * self.PixelSpacing[0]

                self.OptiCT = numpy.zeros_like(self.ctmap, dtype = numpy.int32)
                try:
                    self.Prescription_dose = self.dsplan.DoseReferenceSequence[0].TargetPrescriptionDose / self.dsplan.FractionGroupSequence[0].NumberofFractionsPlanned
                except:
                    self.Prescription_dose = 1000.0

                for i in range(0,len(self.dsstruct.RTROIObservationsSequence)):
                    if self.dsstruct.RTROIObservationsSequence[i].RTROIInterpretedType == 'EXTERNAL':
                        for j in range(0, len(self.dsstruct.StructureSetROISequence)):
                            if self.dsstruct.RTROIObservationsSequence[i].ReferencedROINumber == self.dsstruct.StructureSetROISequence[j].ROINumber:
                                self.ROIExternalName = self.dsstruct.StructureSetROISequence[j].ROIName
                                self.contour_obj.Check_ROI_Name(self.ROIExternalName, self.dsstruct)
                                break
                        if self.External_check.checkState() == Qt.Checked:
                            for zvals in range(0, self.ctmap.shape[0]):
                                self.contour_obj.Contour_Line_Construction(zvals, self.minloc, 0, 0, 0, self.position, self.proneorsurf, self.size, self.PixelSpacing)
                                for c_lines in range(0, len(self.contour_obj.tot_contour)):
                                    CT.contourchange(self.contour_obj.tot_contour[c_lines][0], self.contour_obj.tot_contour[c_lines][1], self.OptiCT, zvals, self.PixelSpacing, 1)
                            CT.OuterSkin(self.ctmap, self.OptiCT)
                            del self.OptiCT

                # ROI List add to the List
                for i in range(0, len(self.contour_obj.ROI_Name_List)):
                    self.item = QListWidgetItem(self.contour_obj.ROI_Name_List[i])
                    self.item.setFlags(self.item.flags() | Qt.ItemIsUserCheckable)
                    self.item.setCheckState(Qt.Unchecked)
                    self.ROIList.addItem(self.item)
                if self.Excheck.count() == 0:
					self.item = QListWidgetItem('Done')
					self.Excheck.addItem(self.item)
        else:
            QMessageBox.about(self, "Error Message", "All required dicoms are not loaded")
    def flgen_clicked(self):
        if self.PLcheck.count() == 1:
            FG.dcm4fluka(self.dsplan,[self.row * self.PixelSpacing[0] ,self.column * self.PixelSpacing[1]], self.proneorsurf, self.savePath)
            QMessageBox.about(self, "Finished", "TXT files are generated.")
        else:
            QMessageBox.about(self, "Error Message", "RT Plan file is not loaded")
            
    def ctout_clicked(self):
        # Directory Creation if not exist.
        f = open(self.savePath + 'imagect', 'w')
        f.write('CORRFACTout.head.int.dat' + '\n')
        f.write('%10.5f %10.5f %10.5f \n'%(self.position[0] / 10, self.position[1] / 10, self.minloc / 10))
        f.write('%d %d %d \n'%(self.ctmap.shape[2], self.ctmap.shape[1], self.ctmap.shape[0]))
        f.write('%7.5f %7.5f %7.5f \n'%(self.PixelSpacing[0] / 10, self.PixelSpacing[1] / 10, self.PixelSpacing[2] / 10))
        if self.proneorsurf == -1:
            for z in range(0, self.ctmap.shape[0]):
                for x in range(self.ctmap.shape[2] - 1, -1, -1):
                    for y in range(self.ctmap.shape[1] - 1, -1, -1):
                        f.write(str(int(self.ctmap[z, y, x])) + '\n')
        elif self.proneorsurf == 1:
            for z in range(0, self.ctmap.shape[0]):
                for x in range(0, self.ctmap.shape[2]):
                    for y in range( 0, self.ctmap.shape[1]):
                        f.write(str(int(self.ctmap[z, y, x])) + '\n')
        f.close()
        QMessageBox.about(self, "Finished", "Files are generated.")
        #numpy.savetxt('imagect', self.txtarray , fmt = '%i', delimiter= '\n', header='test\ntest2\ntest3')
    def roiout_clicked(self):
        for sequence in range(0, len(self.dsplan.IonBeamSequence)):
            self.AGANTRY = int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].GantryAngle)
            self.ACOUCH = int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].PatientSupportAngle)
            if self.proneorsurf == 1:
                self.AROLL = int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].TableTopRollAngle)
            elif self.proneorsurf == -1:
                if int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].TableTopPitchAngle) > 180:
                    self.AROLL = int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].TableTopRollAngle) - 180
                else:
                    self.AROLL = int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].TableTopRollAngle) + 180
            self.APITCH = int(self.dsplan.IonBeamSequence[sequence].IonControlPointSequence[0].TableTopPitchAngle)
            self.AZANGLE = 90 - self.ACOUCH
            self.AGANGLE = 90 - self.AGANTRY
            self.ARANGLE = 360 - self.AROLL
            self.Header_scoring = 'ROT-DEFI  '
            self.score_file = open(self.savePath + 'SCORING_' + str(sequence + 1) + '.flk', 'w')
            self.ROI_name_match = open(self.savePath + 'ROI_Name_Match.txt', 'w')
            self.Name_number = -1
            self.Name_number_2 = -1
            self.Checked_ROI_Name_List = []
            self.input_fluence = float(self.FLUENCE.text())
            self.score_file.write(self.Header_scoring + '0.2010E+03' + '{:.4E}'.format(Decimal(self.AGANGLE)) + '0.0000E+000.0000E+000.0000E+000.0000E+00TRANSFOR' + '\n')
            self.score_file.write(self.Header_scoring + '0.1010E+030.1800E+03' + '{:.4E}'.format(Decimal(self.AZANGLE)) + '0.0000E+000.0000E+000.0000E+00TRANSFOR' + '\n')
            if self.AROLL != 0:
                if self.AROLL >= 0:
                    self.score_file.write(self.Header_scoring + '0.3010E+030.0000E+03' + '{:.4E}'.format(Decimal(self.ARANGLE)) + '0.0000E+000.0000E+000.0000E+00TRANSFOR' + '\n')
                else:
                    self.score_file.write(self.Header_scoring + '0.3010E+030.0000E+03' + '{:.4E}'.format(Decimal(self.AROLL)) + '0.0000E+000.0000E+000.0000E+00TRANSFOR' + '\n')
            if self.APITCH != 0:
                self.score_file.write(self.Header_scoring + '0.2010E+030.0000E+03' + '{:.4E}'.format(Decimal(self.APITCH)) + '0.0000E+000.0000E+000.0000E+00TRANSFOR' + '\n')

            self.numbertag = -1
            self.numbertag_offset = 0
            self.numbertag_ROI_end = ' '
            if self.button_252.isChecked():
                self.numbertag = 252
                self.numbertag_offset = 0
                self.numbertag_ROI_end = ' '
            elif self.button_253.isChecked():
                self.numbertag = 253
                self.numbertag_offset = -10
                self.numbertag_ROI_end = 'A'
            elif self.button_254.isChecked():
                self.numbertag = 254
                self.numbertag_offset = -20
                self.numbertag_ROI_end = 'B'
            for index in range(self.ROIList.count()):
                if self.ROIList.item(index).checkState() == Qt.Checked:
                    self.Name_number += 1
                    self.Checked_ROI_Name_List.append(self.ROIList.item(index).text())
            self.header_1 = '{:8}'.format('ROTPRBIN')
            self.header_input_1 = '{:>3}.{:10}'.format('0', '0000E+00')
            self.header_input_2 = '{:22}'.format('TRANSFOR0.0000E+00')
            self.header_input_3 = '{:10}'.format('ROI0' + str(self.numbertag_ROI_end))
            self.header_input_4 = '{:6}'.format('ROI' + str(self.Name_number) + str(self.numbertag_ROI_end))
            self.header_input_5 = '.1000E+01' + '\n'
            self.header_input_2_1 = '{:>12}'.format('0.0')
            self.header_input_2_2 = '{:>10}'.format('0.0')
            self.header_input_2_3 = '{:>8}.{:3}'.format(str(int(self.input_fluence)), '0')
            self.header_input_2_4 = '{:12}'.format('ROI0' + str(self.numbertag_ROI_end))
            self.header_input_2_5 = '{:13}'.format('ROI' + str(self.Name_number) + str(self.numbertag_ROI_end))
            self.header_input_2_6 = '1.0' + '\n'
            self.score_file.write(self.header_1 + self.header_input_1 + self.header_input_2 + self.header_input_3 + self.header_input_4 + self.header_input_5)
            self.score_file.write(self.header_1 + self.header_input_2_1 + self.header_input_2_2 + self.header_input_2_3 + self.header_input_2_4 + self.header_input_2_5 + self.header_input_2_6)
            for i in range(0, len(self.Checked_ROI_Name_List)):
                self.ROI_name_match.write('ROI' + str(i) + self.numbertag_ROI_end + ' is ' + self.Checked_ROI_Name_List[i] + '\n')
            self.ROI_name_match.close()

            for index in range(self.ROIList.count()):
                if self.ROIList.item(index).checkState() == Qt.Checked:
                    self.ROI_name_out = str(self.ROIList.item(index).text())
                    for j in range(0, len(self.dsstruct.StructureSetROISequence)):
                        if str(self.ROIList.item(index).text()) == self.dsstruct.StructureSetROISequence[j].ROIName:
                            for i in range(0,len(self.dsstruct.RTROIObservationsSequence)):
                                if self.dsstruct.RTROIObservationsSequence[i].ReferencedROINumber == self.dsstruct.StructureSetROISequence[j].ROINumber:
                                    if self.dsstruct.RTROIObservationsSequence[i].RTROIInterpretedType in ['PTV', 'GTV', 'CTV']:
                                        self.Target_tag = 1
                                        print(self.ROI_name_out + ' is a Target.')
                                    else:
                                        self.Target_tag = -1
                                        print(self.ROI_name_out + ' is not a Target.')
                    self.contour_obj.Check_ROI_Name(self.ROI_name_out, self.dsstruct)
                    for zvals in range(0, self.ctmap.shape[0]):
                        self.contour_obj.Contour_Line_Construction(zvals, self.minloc, 0, 0, 0, self.position, self.proneorsurf, self.size, self.PixelSpacing)
                        if len(self.contour_obj.tot_contour) > 0:
                            if self.ROI_z_max < zvals:
                                self.ROI_z_max = zvals
                            if self.ROI_z_min > zvals:
                                self.ROI_z_min = zvals
                        for c_lines in range(0, len(self.contour_obj.tot_contour)):
                            self.temp_x_min = min(self.contour_obj.tot_contour[c_lines][0])
                            self.temp_x_max = max(self.contour_obj.tot_contour[c_lines][0])
                            self.temp_y_min = min(self.contour_obj.tot_contour[c_lines][1])
                            self.temp_y_max = max(self.contour_obj.tot_contour[c_lines][1])
                            if self.temp_x_min < self.ROI_x_min:
                                self.ROI_x_min = self.temp_x_min
                            if self.temp_x_max > self.ROI_x_max:
                                self.ROI_x_max = self.temp_x_max
                            if self.temp_y_min < self.ROI_y_min:
                                self.ROI_y_min = self.temp_y_min
                            if self.temp_y_max > self.ROI_y_max:
                                self.ROI_y_max = self.temp_y_max

                    self.ROI_z_grid_max = int(self.ROI_z_max)
                    if self.ROI_z_grid_max > self.ctmap.shape[0]:
                        self.ROI_z_grid_max = self.ctmap.shape[0]
                    self.ROI_z_grid_min = int(self.ROI_z_min)
                    if self.ROI_z_grid_min < 0:
                        self.ROI_z_grid_min = 0

                    self.ROI_y_grid_max = int(self.ROI_y_max / self.PixelSpacing[1] + 0.5)
                    if self.ROI_y_grid_max > self.ctmap.shape[1]:
                        self.ROI_y_grid_max = self.ctmap.shape[1]
                    self.ROI_y_grid_min = int(self.ROI_y_min / self.PixelSpacing[1] + 0.5)
                    if self.ROI_y_grid_min < 0:
                        self.ROI_y_grid_min = 0

                    self.ROI_x_grid_max = int(self.ROI_x_max / self.PixelSpacing[0] + 0.5)
                    if self.ROI_x_grid_max > self.ctmap.shape[2]:
                        self.ROI_x_grid_max = self.ctmap.shape[2]
                    self.ROI_x_grid_min = int(self.ROI_x_min / self.PixelSpacing[0] + 0.5)
                    if self.ROI_x_grid_min < 0:
                        self.ROI_x_grid_min = 0
                    self.Name_number_2 += 1
                    self.line_header = '{:8}'.format('EVENTBIN')
                    self.line_input_1_1 = '{:>5}.{:4}'.format('-10','0000')
                    self.line_input_1_2 = '{:>8}.{:3}'.format(str(self.numbertag), '000')
                    self.line_input_1_3 = '{:>6}.{:3}'.format(str(-50 - self.Name_number + self.numbertag_offset), '000')
                    '''self.line_input_1_4 = '{:.4E}'.format(Decimal(self.ROI_y_max))
                    self.line_input_1_5 = '{:.4E}'.format(Decimal(self.ROI_x_max))
                    self.line_input_1_6 = '{:.3E}'.format(Decimal(self.ROI_z_max))'''

                    self.line_input_1_4 = '{:{width}.{pref}f}'.format((self.ROI_y_grid_min - 0.5) * 0.1 * self.PixelSpacing[1] * -1 - self.iso_IPP_corrected[1] * 0.1 * -1, width = 11, pref = 4)
                    self.line_input_1_6 = '{:{width}.{pref}f}'.format((self.ROI_z_min - 0.5) * self.PixelSpacing[2] * 0.1 * -1 - self.iso_IPP_corrected[2] * 0.1 * -1, width = 11, pref = 4)
                    self.line_input_1_5 = '{:{width}.{pref}f}'.format((self.ROI_x_grid_max + 0.5) * 0.1 * self.PixelSpacing[0] - self.iso_IPP_corrected[0] * 0.1, width = 10, pref = 4)
                    self.line_input_1_7 = '{:8}'.format('ROI' + str(self.Name_number_2) + str(self.numbertag_ROI_end)) + '\n'
                    self.score_file.write(self.line_header + self.line_input_1_1 + self.line_input_1_2 + self.line_input_1_3 + self.line_input_1_4 + self.line_input_1_5 + self.line_input_1_6 + ' ' + self.line_input_1_7)
                    '''self.line_input_2_1 = '{:.4E}'.format(Decimal(self.ROI_y_min))
                    self.line_input_2_2 = '{:.5E}'.format(Decimal(self.ROI_x_min))
                    self.line_input_2_3 = '{:.5E}'.format(Decimal(self.ROI_z_min))'''
                    self.line_input_2_2 = '{:{width}.{pref}f}'.format((self.ROI_x_grid_min - 0.5) * 0.1 * self.PixelSpacing[0] - self.iso_IPP_corrected[0] * 0.1, width = 10, pref = 4)
                    self.line_input_2_1 = '{:{width}.{pref}f}'.format((self.ROI_y_grid_max + 0.5) * 0.1 * self.PixelSpacing[1] * -1 - self.iso_IPP_corrected[1] * 0.1 * -1, width = 10, pref = 4)
                    self.line_input_2_3 = '{:{width}.{pref}f}'.format((self.ROI_z_max +0.5) * self.PixelSpacing[2] * 0.1 * - 1 - self.iso_IPP_corrected[2] * 0.1 * - 1, width = 9, pref = 4)
                    self.line_input_2_4 = '{:>5}.{:4}'.format((self.ROI_y_grid_max - self.ROI_y_grid_min + 1), '0000')
                    self.line_input_2_5 = '{:>6}.{:3}'.format((self.ROI_x_grid_max - self.ROI_x_grid_min + 1), '000')
                    self.line_input_2_6 = '{:>4}.{:4}'.format((self.ROI_z_grid_max - self.ROI_z_grid_min + 1), '0000')
                    self.line_input_2_7 = '{:>6}'.format('&') + '\n'
                    self.score_file.write(self.line_header + self.line_input_2_1 + self.line_input_2_2 + self.line_input_2_3 + self.line_input_2_4 + self.line_input_2_5 + self.line_input_2_6 + self.line_input_2_7)
                    #self.score_file.write('EVENTBIN  -10.0000     ' + str(self.numbertag) + '   ' + str(-50 - self.Name_number + self.numbertag_offset) + ' ' + str(self.ROI_y_max) + ' ' + str(self.ROI_x_max) + ' ' + str(self.ROI_z_max * self.PixelSpacing[2]) + ' ROI' + str(self.Name_number_2) + str(self.numbertag_ROI_end) + '\n')
                    #self.score_file.write('EVENTBIN  ' + str(self.ROI_y_min) + ' ' + str(self.ROI_x_min) + ' ' + str(self.ROI_z_min * self.PixelSpacing[2]) + ' ' + str(self.ROI_y_grid_max - self.ROI_y_grid_min) + ' ' + str(self.ROI_x_grid_max - self.ROI_x_grid_min) + ' ' + str(self.ROI_z_grid_max - self.ROI_z_grid_min) + ' &' + '\n')
                    if sequence == 0:
                        print("ROI name is : " + self.ROI_name_out)
                        print("X max : " + str(self.ROI_x_grid_max) + " X min : " + str(self.ROI_x_grid_min))
                        print("Y max : " + str(self.ROI_y_grid_max) + " Y min : " + str(self.ROI_y_grid_min))
                        print("Z max : " + str(self.ROI_z_grid_max) + " Z min : " + str(self.ROI_z_grid_min))
                        print('Prescription Dose : ' + str(self.Prescription_dose))
                        self.temp_voxel_value = 0
                        self.ROI_out_array = self.ctmap[self.ROI_z_grid_min:self.ROI_z_grid_max + 1, self.ROI_y_grid_min: self.ROI_y_grid_max + 1, self.ROI_x_grid_min: self.ROI_x_grid_max + 1]
                        self.ROI_file_name = self.savePath + self.ROI_name_out
                        f = open(self.ROI_file_name + '_temp.dat', 'w')
                        f.write('# ' + str(self.ROI_name_out) + '\n')
                        f.write(' ' + str(self.ROI_y_grid_max - self.ROI_y_grid_min + 1) + ' ' + str(self.ROI_x_grid_max - self.ROI_x_grid_min + 1) + ' ' + str(self.ROI_z_grid_max - self.ROI_z_grid_min + 1) + '\n')
                        #f.write(' ' + str((- self.iso_IPP_corrected[1] + (self.ROI_y_grid_min - 0.5) * self.PixelSpacing[1]) / 10) + ' ' +  str((- self.iso_IPP_corrected[0] + (self.ROI_x_grid_min - 0.5) * self.PixelSpacing[0]) / 10) + ' ' + str((- self.iso_IPP_corrected[2] + (self.ROI_z_grid_min - 0.5) * self.PixelSpacing[2]) / 10) + '\n')
                        #f.write(' ' + str((- self.iso_IPP_corrected[1] + (self.ROI_y_grid_max + 0.5) * self.PixelSpacing[1]) / 10) + ' ' +  str((- self.iso_IPP_corrected[0] + (self.ROI_x_grid_max + 0.5) * self.PixelSpacing[0]) / 10) + ' ' + str((- self.iso_IPP_corrected[2] + (self.ROI_z_grid_max + 0.5) * self.PixelSpacing[2]) / 10) + '\n')
                        #f.write(' ' + str(-1 * (- self.iso_IPP_corrected[1] + (self.ROI_y_grid_max - 0.5) * self.PixelSpacing[1]) / 10) + ' ' +  str((- self.iso_IPP_corrected[0] + (self.ROI_x_grid_min - 0.5) * self.PixelSpacing[0]) / 10) + ' ' + str(-1 * (- self.iso_IPP_corrected[2] + (self.ROI_z_grid_max + 0.5) * self.PixelSpacing[2]) / 10) + '\n')
                        #f.write(' ' + str(-1 * (- self.iso_IPP_corrected[1] + (self.ROI_y_grid_min + 0.5) * self.PixelSpacing[1]) / 10) + ' ' +  str((- self.iso_IPP_corrected[0] + (self.ROI_x_grid_max + 0.5) * self.PixelSpacing[0]) / 10) + ' ' + str(-1 * (- self.iso_IPP_corrected[2] + (self.ROI_z_grid_min - 0.5) * self.PixelSpacing[2]) / 10) + '\n')
                        f.write(' ' + self.line_input_2_1 + ' ' +  self.line_input_2_2 + ' ' + self.line_input_2_3 + '\n')
                        f.write(' ' + self.line_input_1_4 + ' ' +  self.line_input_1_5 + ' ' + self.line_input_1_6 + '\n')
                        f.write(' ' + str(0) + '\n')
                        self.ROI_Voxel_num = 0
                        if self.proneorsurf == -1:
                            for z in range(0, self.ROI_out_array.shape[0]):
                                self.contour_obj.Contour_Line_Construction(z + self.ROI_z_grid_min, self.minloc, 0, 0, 0, self.position,self.proneorsurf, self.size, self.PixelSpacing)
                                for c_lines in range(0, len(self.contour_obj.tot_contour)):
                                    Pointsets = []
                                    for k in range(0, len(self.contour_obj.tot_contour[c_lines][0])):
                                        Pointsets.append((self.contour_obj.tot_contour[c_lines][0][k], self.contour_obj.tot_contour[c_lines][1][k]))
                                    poly = Polygon(Pointsets)
                                    #for x in range(self.ROI_out_array.shape[2] - 1, -1, -1):
                                    for x in range(0, self.ROI_out_array.shape[2]):
                                        for y in range(0, self.ROI_out_array.shape[1]):
                                            if poly.contains(shapely.geometry.Point((x + self.ROI_x_grid_min) * self.PixelSpacing[0], (y + self.ROI_y_grid_min) * self.PixelSpacing[1])):
                                                self.temp_voxel_value = y + self.ROI_out_array.shape[1] * x + self.ROI_out_array.shape[1] * self.ROI_out_array.shape[2] * z
                                                f.write(str(self.temp_voxel_value) + ' ' + str(self.temp_voxel_value) + ' ' + str(self.Prescription_dose) + ' ' + str(self.Target_tag) + ' ' + str(1) + '\n')
                                                self.ROI_Voxel_num += 1
                        elif self.proneorsurf == 1:
                            for z in range(0, self.ROI_out_array.shape[0]):
                                self.contour_obj.Contour_Line_Construction(z + self.ROI_z_grid_min, self.minloc, 0, 0, 0, self.position,self.proneorsurf, self.size,self.PixelSpacing)
                                for c_lines in range(0, len(self.contour_obj.tot_contour)):
                                    Pointsets = []
                                    for k in range(0, len(self.contour_obj.tot_contour[c_lines][0])):
                                        Pointsets.append((self.contour_obj.tot_contour[c_lines][0][k], self.contour_obj.tot_contour[c_lines][1][k]))
                                    poly = Polygon(Pointsets)
                                    for x in range(0, self.ROI_out_array.shape[2]):
                                        for y in range(self.ROI_out_array.shape[1], -1, -1):
                                            if poly.contains(shapely.geometry.Point((x + self.ROI_x_grid_min) * self.PixelSpacing[0], (y + self.ROI_y_grid_min) * self.PixelSpacing[1])):
                                                self.temp_voxel_value = (self.ROI_out_array.shape[1] - 1 - y) + self.ROI_out_array.shape[1] * x + self.ROI_out_array.shape[1] * self.ROI_out_array.shape[2] * (self.ROI_out_array.shape[0] -1 - z)
                                                f.write(str(self.temp_voxel_value) + ' ' + str(self.temp_voxel_value) + ' ' + str(self.Prescription_dose) + ' ' + str(self.Target_tag) + ' ' + str(1) + '\n')
                                                self.ROI_Voxel_num += 1
                        f.close()
                        f = open(self.ROI_file_name + '_temp.dat', 'r')
                        g = open(self.ROI_file_name + '.dat', 'w')
                        self.line_tag = 0
                        self.line = f.readline()
                        while(self.line):
                            if self.line_tag == 4:
                                g.write(str(self.ROI_Voxel_num) + '\n')
                            else:
                                g.write(self.line)
                            self.line = f.readline()
                            self.line_tag += 1
                        f.close()
                        g.close()
                        os.remove(self.ROI_file_name + '_temp.dat')
            self.score_file.close()

        QMessageBox.about(self, "Finished", "Files are generated.")
    def HUCH_clicked(self):
        # Check the numbers of ROI checked
        self.RTmap = numpy.zeros(self.ctmap.shape, numpy.int32)
        CT.matchanger(self.ctmap,self.RTmap)
        self.numclicked = 0 # Checksum for the ROI checked
        self.valchange = int(self.valch.text())
        for index in range(self.ROIList.count()):
            if self.ROIList.item(index).checkState() == Qt.Checked:
                self.numclicked = self.numclicked + 1
        if self.numclicked == 0:
            QMessageBox.about(self, "File load", "No ROI is checked")
        elif self.numclicked > 1:
            QMessageBox.about(self, "File load", "Check only one ROI")
        else:
            for index in range(self.ROIList.count()):
                if self.ROIList.item(index).checkState() == Qt.Checked:
                    for i in range(0,len(self.contour_obj.ROI_Name_List)):
                        if self.contour_obj.ROI_Name_List[i] == self.ROIList.item(index).text():
                            self.contour_obj.Check_ROI_Name(self.ROIList.item(index).text(), self.dsstruct)
                            break
                    for zvals in range(0, self.ctmap.shape[0]):
                        self.contour_obj.Contour_Line_Construction(zvals, self.minloc, 0, 0, 0, self.position, self.proneorsurf, self.size, self.PixelSpacing)
                        for c_lines in range(0, len(self.contour_obj.tot_contour)):
                            CT.contourchange(self.contour_obj.tot_contour[c_lines][0], self.contour_obj.tot_contour[c_lines][1], self.ctmap, zvals, self.PixelSpacing, self.valchange)

    def valchange(self):
        if self.Excheck.count() == 0:
            QMessageBox.about(self, "File load", "Load files first")
        else:
            self.Dataset = []  # Point dataset
            self.linex = []  # X points array
            self.liney = []  # Y points array
            self.colors = []  # color set
            self.lineset = []  # Line with x and y
            self.scene.clear()  # Reset scene for the clear view
            self.vals = self.CTSlider.value() # Get number of slide from slider
            self.SliceNumber.setText(str(self.vals + 1)) # Show the number at textbox        
            
            self.CTSlider.setMinimum(0)  # Set min of Slide
        
            if True:
                self.imagefactor = 1
                self.CTSlider.setMaximum(self.ctmap.shape[0]-1)  # Set max of Slide
                self.Temparray = self.ctmap[self.vals,:,:]
                
                self.Pixelsize = [self.PixelSpacing[0] * self.Temparray.shape[0], self.PixelSpacing[1] * self.Temparray.shape[1]]
                self.imgsh = qimage2ndarray.gray2qimage(self.Temparray,normalize = True)  # Change 2D CT array to Qimage
                
                self.pixemap = QPixmap.fromImage(self.imgsh.scaled(self.Pixelsize[0] , self.Pixelsize[1]))
                self.scene.setSceneRect(0, 0, self.Pixelsize[0] , self.Pixelsize[1])
                self.scene.addPixmap(self.pixemap.scaled(self.Pixelsize[0] ,self.Pixelsize[1]))

                for index in range(self.ROIList.count()):
                    if self.ROIList.item(index).checkState() == Qt.Checked:
                        self.contour_obj.Check_ROI_Name(str(self.ROIList.item(index).text()), self.dsstruct)
                        self.contour_obj.Contour_Line_Construction(self.vals, self.minloc, 0, 0, 0, self.position, self.proneorsurf, self.size, self.PixelSpacing)
                        for cont in range(0, len(self.contour_obj.tot_contour)):
                            for list in range(0, len(self.contour_obj.tot_contour[cont][0]) - 1):
                                self.scene.addLine(QLineF(self.contour_obj.tot_contour[cont][0][list], self.contour_obj.tot_contour[cont][1][list], self.contour_obj.tot_contour[cont][0][list + 1], self.contour_obj.tot_contour[cont][1][list + 1]), QPen(self.contour_obj.color, 3))
                            self.scene.addLine(QLineF(self.contour_obj.tot_contour[cont][0][-1],
                                                      self.contour_obj.tot_contour[cont][1][-1],
                                                      self.contour_obj.tot_contour[cont][0][0],
                                                      self.contour_obj.tot_contour[cont][1][0]),
                                               QPen(self.contour_obj.color, 3))


                self.CTViewer.setScene(self.scene)
                self.CTViewer.show()

    def undo_clicked(self):
        CT.matchanger(self.RTmap, self.ctmap)

    def Raster_clicked(self):
        #self.cpath = os.path.dirname(os.path.abspath(__file__))  # Current path
        #self.savePath = self.cpath + '/Files/'  # Save Directory
        self.filenamestr = 'raster_'
        self.particlenum = int(self.FLUENCE.text())  # Get Particle numbers
        self.Numcore = int(self.ThreadSlide.value())  # Core number
        self.Beamtocore = int(self.Numcore / len(self.dsplan.IonBeamSequence))  # Core/Beam

        # Loop in beam
        for beam in range(0, len(self.dsplan.IonBeamSequence)):
            self.Numsquence = 0  # Total squence number

            # Particle Type
            if self.dsplan.IonBeamSequence[0].RadiationType == 'PROTON':
                self.mass = 1
                self.charge = 1
                self.effactor = 1.0 / 4.0
            elif self.dsplan.IonBeamSequence[0].RadiationType == 'ION':
                self.mass = 12
                self.charge = 6
                self.effactor = 9.0
            self.beamname = '_beam' + str(beam + 1) + '.txt'
            self.Sequelist = []
            self.xposition = []
            self.yposition = []
            self.NBNRG = []
            self.SpotTuneID = []
            self.SSPWeight = []

            # Gather all informations in array
            for squence in range(0, int(len(self.dsplan.IonBeamSequence[beam].IonControlPointSequence))):
                if squence % 2 == 0:
                    self.Numsquence = len(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotPositionMap) / 2 + self.Numsquence
                    for points in range(0, int(len(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotPositionMap) / 2)):
                        self.xposition.append(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotPositionMap[points * 2])
                        self.yposition.append(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotPositionMap[points * 2 + 1])
                        self.SpotTuneID.append(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotTuneID)
                        self.NBNRG.append(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].NominalBeamEnergy)
                        if self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].NumberOfScanSpotPositions == 1:
                            self.SSPWeight.append(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotMetersetWeights)
                        else:
                            self.SSPWeight.append(self.dsplan.IonBeamSequence[beam].IonControlPointSequence[squence].ScanSpotMetersetWeights[points])

            self.beamtonum = int(self.Numsquence / self.Beamtocore)  # Total points / core per beam

            # Construct the array of parallel numbers
            for corenum in range(0, self.Beamtocore):
                self.Sequelist.append(self.beamtonum)

            self.checkpar = self.Numsquence - sum(self.Sequelist)  # Get the rest of numbers

            # Rest of the numbers
            for nums in range(0, int(self.checkpar)):
                self.Sequelist[nums] = self.Sequelist[nums] + 1
            self.tester = 0  # checksum integer
            self.tester2 = 0  # integer for the parallel

            # Print all the information
            for runs in range(0, len(self.Sequelist)):
                if runs < 9:
                    self.totfilename = self.savePath + self.filenamestr + '0' + str(runs + 1) + self.beamname
                else:
                    self.totfilename = self.savePath + self.filenamestr + str(runs + 1) + self.beamname
                self.tester2 = self.tester2 + self.Sequelist[runs]
                f = open(self.totfilename, 'w')
                f.write(str(self.Sequelist[runs]) + ' ' + str(1) + '\n')
                while (self.tester < self.tester2):
                    f.write(' ' + str(self.tester + 1) + ' ')
                    f.write("%6.3f %6.3f %6.2f %6.3f %d %d %10.1f %d" % (self.xposition[self.tester] / 10, self.yposition[self.tester] / 10, self.NBNRG[self.tester], float(self.SpotTuneID[self.tester]) / 10, self.charge, self.mass, self.SSPWeight[self.tester],self.particlenum))
                    f.write('   ' + str(beam + 1) + '\n')
                    self.tester = self.tester + 1
                f.close()
        QMessageBox.about(self, "Information", "Fluka file generation finished.")
    def core_display(self):
        self.Thread_display.setText(str(self.ThreadSlide.value()))

if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    myWindow = Mainwindow()
    myWindow.show()
    app.exec_()
