import sys
import os
import platform
import math
import shutil
from os.path import join as pjoin
from decimal import Decimal

# PyQt4 import
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import PyQt4.Qt
from PyQt4 import uic
from PyQt4.QtGui import QImage

# Matplotlib and numpy
import numpy
import numpy.ma as ma

# Other module import
import qimage2ndarray
import dicom
import shapely
from shapely.geometry import Polygon
import shapely.speedups

# Built functions
import ReadDicom as RD
import StructSet as SS
import FlukaGeneration as FG
import struct

# UI load form
form_class = uic.loadUiType('ROI_reader.ui')[0]

class Mainwindow(QMainWindow, form_class):
    def __init__(self):
        super(Mainwindow, self).__init__()
        self.setupUi(self)
        self.connect(self.ROI, SIGNAL("clicked()"), self.ROI_process)
        self.CTSlider.valueChanged.connect(self.valchange)
        self.ROI_path = ''
        self.file_open = ''
        self.ROI_name = ''
        self.ROI_x_size = 1
        self.ROI_y_size = 1
        self.ROI_z_size = 1
        self.temp_voxel = 0
        self.templine = ''
        self.ROI_array = numpy.zeros((self.ROI_z_size,self.ROI_y_size,self.ROI_x_size), numpy.int16)
        self.coordinate_save = []
        self.CTSlider.setMinimum(0)
        self.scene = QGraphicsScene()
        self.dvhviewer = QGraphicsView()
    def ROI_process(self):
        self.ROI_path = str(QFileDialog.getOpenFileName(self))  # Load CT path with file browser
        self.ROI_name = ''
        self.coordinate_save = []
        self.file_open = open(self.ROI_path, 'r')
        self.templine = self.file_open.readline()
        for i in range(0, len(self.templine.split())):
            if i == 0:
                pass
            else:
                self.ROI_name += self.templine.split()[i]
        self.templine = self.file_open.readline()
        self.ROI_y_size = int(self.templine.split()[0])
        self.ROI_x_size = int(self.templine.split()[1])
        self.ROI_z_size = int(self.templine.split()[2])
        self.ROI_array = numpy.zeros((self.ROI_z_size,self.ROI_y_size,self.ROI_x_size), numpy.int16)

        self.templine = self.file_open.readline()
        self.templine = self.file_open.readline()
        self.templine = self.file_open.readline()
        self.templine = self.file_open.readline()
        while(self.templine):
            self.coordinate_save.append(float(self.templine.split()[0]))
            self.templine = self.file_open.readline()
        for z in range(0, self.ROI_z_size):
            for y in range(0, self.ROI_y_size):
                for x in range(0, self.ROI_x_size):
                    self.temp_voxel = y + x * self.ROI_y_size + z * self.ROI_y_size * self.ROI_x_size
                    if self.temp_voxel in self.coordinate_save:
                        self.ROI_array[z, y, x] = 1

        self.CTSlider.setMaximum(self.ROI_z_size - 1)
    def valchange(self):
        self.scene.clear()  # Reset scene for the clear view
        self.vals = self.CTSlider.value()  # Get number of slide from slider
        self.SliceNumber.setText(str(self.vals + 1))  # Show the number at textbox

        self.Temparray = self.ROI_array[self.vals, :, :]
        self.imgsh = qimage2ndarray.gray2qimage(self.Temparray, normalize=True)  # Change 2D CT array to Qimage

        self.pixemap = QPixmap.fromImage(self.imgsh.scaled(512, 512))
        self.scene.setSceneRect(0, 0, 512, 512)
        self.scene.addPixmap(self.pixemap.scaled(512, 512))

        self.CTViewer.setScene(self.scene)
        self.CTViewer.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWindow = Mainwindow()
    myWindow.show()
    app.exec_()