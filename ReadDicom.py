import dicom
import os
dicom.config.enforce_valid_values = False
from os.path import join as pjoin
import shutil
######################################################################################################################
# Dicom file read functions
######################################################################################################################

################################################################
# Creat all the dicom files list from CT directory
#
# Read all dicom from CT Directory under patient directory
# Default path for automatic run is CT under patient directory
# Return all the list of dicom files in the Directory
################################################################
def dcmRead(path):
    dcmList = []
    for dirName, subdirList, fileList in os.walk(path):
        for filename in fileList:
            dcmList.append(os.path.join(dirName, filename))
        break
    return dcmList

################################################################
# Read All the Dicoms form the subdirectory Treatdcm
#
# It returns the array of dicom file names
# It is different from the dcmRead() that it read dicoms from Treatdcm directory
################################################################
def dcmReadAll():
    dcmList = []
    cpath = os.path.dirname(os.path.abspath( __file__ ))
    ctpath = cpath + "/Treatdcm/"
    for dirName, subdirList, fileList in os.walk(ctpath):
        for filename in fileList:
            if ".dcm" in filename.lower():
                dcmList.append(os.path.join(dirName, filename))
    return dcmList

################################################################
# Read All the CT Dicoms form the Treatdcm directory
#
# It returns the array of CT dicom file names
################################################################
def readCTdcm(lstFilesDCM):
    lstFilesCT = []
    for lstFiles in lstFilesDCM:
        ds = dicom.read_file(lstFiles,force =True)
        if ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.2': #CT Image
            lstFilesCT.append(os.path.join(lstFiles))
    return lstFilesCT

################################################################
# Read All the RT Dose Dicoms form the Treatdcm directory
#
# It returns the array of Dose dicom file names
################################################################
def readRTDdcm(lstFilesDCM):
    lstFilesDose = []
    for filenameDCM in lstFilesDCM:
        ds1 = dicom.read_file(filenameDCM, force = True)
        if ds1.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.2': #RT Dose files
            lstFilesDose.append(os.path.join(filenameDCM))
    return lstFilesDose

################################################################
# Read All the RT Dose Dicoms form the Treatdcm directory
#
# It returns the array of RT Plan dicom file names
################################################################
def readRTPdcm():
    # RT_PLAN.dcm read for FractionGroupSequence read
    # Only 1, hard coded.
    dsplan = dicom.read_file('Treatdcm/RT_PLAN.dcm')
    return dsplan

################################################################
# Read All the RT Dose Dicoms form the Treatdcm directory
#
# It returns the array of RT Struct dicom file names
################################################################
def readRTSdcm():
    # RT_Struct.dcm read for FractionGroupSequence read
    # Only 1, hard coded.
    dsstruct = dicom.read_file('Treatdcm/RT_Struct.dcm', force=True)
    return dsstruct

######################################################################################################################
# Sort and rename dicom files.
#
# Dicom Save Function
# Rename all the dicom files depend on the SOPClassUID
# CT images are sorted and renamed with certain continuous numbers.
# RTPLAN is stored with RT_PLAN.dcm
# RTStruct is stored with numbering
# Default path of raw dicom files are /CT/ under Patient directory
# Default path of renamed dicom files are /Treatdcm/ under Patient directory
######################################################################################################################
def dSave(dcmList):
    cpath = os.path.dirname(os.path.abspath( __file__ ))
    ctpath = cpath + "/CT/"
    tester = 0
    listname = []
    listname2 = []
    if not os.path.exists(cpath+"/Treatdcm/"):
        os.makedirs(cpath+"/Treatdcm/")
    for dcmFile in dcmList:
        ds = dicom.read_file(dcmFile,force=True)
        if ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.2': #Returns with CT Image Storage but not a real value
            if ds.InstanceNumber < 10:
                savePath = cpath+"/Treatdcm/"+"CT_Image_00"+str(ds.InstanceNumber)+".dcm"
                ds.save_as(savePath)
            elif ds.InstanceNumber > 9 and ds.InstanceNumber< 100:
                savePath = cpath+"/Treatdcm/"+"CT_Image_0"+str(ds.InstanceNumber)+".dcm"
                ds.save_as(savePath)
            elif  ds.InstanceNumber > 99 and  ds.InstanceNumber <1000:
                savePath = cpath+"/Treatdcm/"+"CT_Image_"+str(ds.InstanceNumber)+".dcm"
                ds.save_as(savePath)
        if ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.8': # RT Ion Plan
            savePath = cpath+"/Treatdcm/RT_PLAN.dcm"
            shutil.copyfile(dcmFile, savePath)
        if ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.3': # RT Structure Set
            savePath = cpath+"/Treatdcm/RT_Struct.dcm"
            ds.save_as(savePath)
        
        if ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.2': # RT Dose 
            # Dose file for all beams
            #if ds.DoseSummationType == 'PLAN':
            savePath = cpath+"/Treatdcm/RT_Dose_"+str(tester)+".dcm"
            ds.save_as(savePath)
            tester = tester+1
    return
