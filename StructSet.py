import os
import dicom
import dicom.config
dicom.config.enforce_valid_values = False
from os.path import join as pjoin
from scipy import interpolate
import string
import math as m
import numpy
import numpy.ma as ma
import shapely
from shapely.geometry import Polygon
import ReadDicom as RD


######################################################################################################################
# ROI Volume Calculation and Structure Contour
######################################################################################################################

##############################################################
# Creat Points array and Find CT image correspond
##############################################################
def GetCTposition(ContourPoints, CTList):
    ReturnPoints = []
    ReturnPointsets = []
    test1 = []
    position = []

    for filename in CTList:
        ds = dicom.read_file(filename)
        if ContourPoints[2] == ds.SliceLocation:
            position = ds.ImagePositionPatient
            CTname = filename
    
    ds = dicom.read_file(CTname)
    ylen = ds.Columns * ds.PixelSpacing[1]
    for i in range(0, len(ContourPoints)):
        if i % 3 == 0:
            ReturnPoints.append(ContourPoints[i]-position[0])
            test1.append(ContourPoints[i]-position[0])
        elif i % 3 == 1:
            ReturnPoints.append(ContourPoints[i]-position[1])
            test1.append(ContourPoints[i]-position[1])
        elif i % 3 == 2:
            ReturnPointsets.append(test1)
            test1 = []
            
    return ReturnPoints, CTname, ReturnPointsets

def thickenss(lstFilesCT):
    lstFilesCT.sort()
    SlicePosition = []
    zval = []
    for k in range(0, len(lstFilesCT)):
        ds = dicom.read_file(lstFilesCT[k])
        SlicePosition.append(ds.SliceLocation)
    for j in range(0, len(SlicePosition)-1):
        zval.append(SlicePosition[j+1] - SlicePosition[j])
    zval.sort()
    zval = list(set(zval))
    return zval[0]

def p2l(Points,CTList):
    returnx =[]
    returny =[]

    dsCT = dicom.read_file(CTList[0])
    for i in range(0, len(Points)):
        if  i % 2 == 0:
            returnx.append(Points[i]/dsCT.PixelSpacing[0])
        else:
            returny.append(Points[i]/dsCT.PixelSpacing[1])
    print('ok')
    returnx.append(Points[0]/dsCT.PixelSpacing[0])
    returny.append(Points[1]/dsCT.PixelSpacing[1])
    
    return returnx, returny

def Points2lines(linex, liney):
    LineP = []
    returnlines = []
    for i in range(0, len(linex)):
        LineP = []
        if not i == (len(linex)-1):
            LineP.append(linex[i])
            LineP.append(liney[i])
            LineP.append(linex[i+1])
            LineP.append(liney[i+1])
            returnlines.append(LineP)
        else:
            LineP.append(linex[i])
            LineP.append(liney[i])
            LineP.append(linex[0])
            LineP.append(liney[0])
            returnlines.append(LineP)
    return returnlines

def ROIdef(dsstruct):
    ROIinput = []
    ROISequence = []
    for i in range(0, len(dsstruct.StructureSetROISequence)):
        ROIinput.append(dsstruct.StructureSetROISequence[i].ROIName)
        for j in range(0, len(dsstruct.StructureSetROISequence)):
            if dsstruct.StructureSetROISequence[i].ROINumber == dsstruct.ROIContourSequence[j].ReferencedROINumber:
                ROISequence.append(dsstruct.ROIContourSequence[j])
    return ROIinput, ROISequence

def findROI(dsstruct, ROINum):
    for i in range(0, len(dsstruct.ROIContourSequence)):
        if dsstruct.ROIContourSequence[i].ReferencedROINumber == ROINum:
            return i
        
def Contourline(returnarray, linex, liney, dsCT):
    temparray = []
    temparray.append(linex)
    temparray.append(liney)
    temparray.append(dsCT.InstanceNumber)
    returnarray.append(temparray)
    return returnarray

def Buildstruct(dsstruct, x, y, thickn, STRarray, ROIstruct, lines):
    cumvol = 0
    lines = []
    colors = []
    color = dsstruct.ROIContourSequence[ROIstruct].ROIDisplayColor
    colors = (float(color[0]/255), float(color[1]/255), float(color[2]/255))
    for contours in range(0, len(dsstruct.ROIContourSequence[ROIstruct].Contours)):
        Points = dsstruct.ROIContourSequence[ROIstruct].Contours[contours].ContourData
        Points, CTName , Pointsets= GetCTposition(Points)
        dsCT = dicom.read_file(CTName)
        linex, liney = p2l(Points)
        lines = Contourline(lines, linex, liney, dsCT)
        poly = Polygon(Pointsets)
        polydat = poly.bounds
        polydat2 = list(polydat)
        for i in range(0, len(polydat2)):
            if i == 0 or i == 1:
                polydat2[i]=int(polydat2[i]/dsCT.PixelSpacing[0])
            else:
                if round(polydat2[i]/dsCT.PixelSpacing[0]) - polydat2[i]/dsCT.PixelSpacing[0] >= 0:
                    polydat2[i]=round(polydat2[i]/dsCT.PixelSpacing[0])
                else:
                    polydat2[i]=round(polydat2[i]/dsCT.PixelSpacing[0])+1
        for j in range(polydat2[1],polydat2[3],1):
            for i in range(polydat2[0], polydat2[2], 1):
                points = shapely.geometry.Point(i*dsCT.PixelSpacing[0],j*dsCT.PixelSpacing[1])
                if poly.contains(points):
                    STRarray[j][i][dsCT.InstanceNumber] = 1
                    cumvol = cumvol + 1
        Uvol = dsCT.PixelSpacing[0]*dsCT.PixelSpacing[1]*thickn /1000
        Tvol = Uvol * cumvol
    return STRarray, Tvol, lines, colors

def DVHsshow(Plandose, Multiarray):
    Doselist = Multiarray[numpy.where(Multiarray > 0)]
    return Doselist
