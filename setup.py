try:

    from distutils.core import setup
    from distutils.extension import Extension
except ImportError:
    from setuptools import setup
    from setuptools import Extension
    
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import numpy
import shapely

from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules=[
    Extension("Contours_64bit",
              ["Contours.pyx"],
              
              #extra_compile_args = ['/openmp' ,"/Ox", "/ffast-math", "/march=native"],
              #extra_link_args=['/openmp']
              ) 
]

setup( 
  name = 'WEPL',
  cmdclass = {"build_ext": build_ext},
    ext_modules = ext_modules,
  include_dirs=[numpy.get_include()]
)
'''setup(

  cmdclass = {'build_ext': build_ext},
  ext_modules=cythonize("WEPL.pyx"),
  include_dirs=[numpy.get_include()]
)'''
